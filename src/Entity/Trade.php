<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table()
 */
class Trade
{
    use TimestampableTrait;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $symbol;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $binanceOrderId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $clientOrderId;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $transactTime;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $origQty;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $executedQty;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $timeInForce;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $side;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $closing;

    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="trades")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Trade
     */
    public function setId(int $id): Trade
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return Trade
     */
    public function setSymbol(string $symbol): Trade
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return int
     */
    public function getBinanceOrderId(): int
    {
        return $this->binanceOrderId;
    }

    /**
     * @param int $binanceOrderId
     * @return Trade
     */
    public function setBinanceOrderId(int $binanceOrderId): Trade
    {
        $this->binanceOrderId = $binanceOrderId;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientOrderId(): string
    {
        return $this->clientOrderId;
    }

    /**
     * @param string $clientOrderId
     * @return Trade
     */
    public function setClientOrderId(string $clientOrderId): Trade
    {
        $this->clientOrderId = $clientOrderId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTransactTime(): int
    {
        return (int) $this->transactTime;
    }

    /**
     * @param int $transactTime
     * @return Trade
     */
    public function setTransactTime(int $transactTime): Trade
    {
        $this->transactTime = $transactTime;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Trade
     */
    public function setPrice(float $price): Trade
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getOrigQty(): float
    {
        return $this->origQty;
    }

    /**
     * @param float $origQty
     * @return Trade
     */
    public function setOrigQty(float $origQty): Trade
    {
        $this->origQty = $origQty;

        return $this;
    }

    /**
     * @return float
     */
    public function getExecutedQty(): float
    {
        return $this->executedQty;
    }

    /**
     * @param float $executedQty
     * @return Trade
     */
    public function setExecutedQty(float $executedQty): Trade
    {
        $this->executedQty = $executedQty;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Trade
     */
    public function setStatus(string $status): Trade
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeInForce(): string
    {
        return $this->timeInForce;
    }

    /**
     * @param string $timeInForce
     * @return Trade
     */
    public function setTimeInForce(string $timeInForce): Trade
    {
        $this->timeInForce = $timeInForce;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Trade
     */
    public function setType(string $type): Trade
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getSide(): string
    {
        return $this->side;
    }

    /**
     * @param string $side
     * @return Trade
     */
    public function setSide(string $side): Trade
    {
        $this->side = $side;

        return $this;
    }

    /**
     * @return bool
     */
    public function isClosing(): bool
    {
        return $this->closing;
    }

    /**
     * @param bool $closing
     * @return Trade
     */
    public function setClosing(bool $closing): Trade
    {
        $this->closing = $closing;

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return Trade
     */
    public function setOrder(Order $order): Trade
    {
        $this->order = $order;

        return $this;
    }
}
