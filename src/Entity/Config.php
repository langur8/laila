<?php declare(strict_types=1);

namespace App\Entity;

use App\Configuration\Trend;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class Config
{
    use TimestampableTrait;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Laila
     * @ORM\OneToOne(targetEntity="Laila", inversedBy="config")
     * @ORM\JoinColumn(name="laila_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $laila;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $running = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $openNewOrders = true;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $demoMode = true;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $claimLeftSide = true;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $trend = Trend::UP;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $analyzeInterval = '1m';

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $analyzeTicks = 5;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $protectedZoneUp = 0.1;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $protectedZoneDown = 0.1;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $orderAmount = 0.0001;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $useMinOrderAmount = true;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $orderAmountPrecision = 6;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $pricePrecision = 2;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $fees = 0.075;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $takeProfitPercentage = 0.3;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $activeOrderCount = 1;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $newOrderShiftPercentage = 0.02;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $newOrderMaxWaitTimeToShift = 300;

    /**
     * @return int
     */
    public function getTrendCoef(): int
    {
        return $this->getTrend() === Trend::DOWN ? -1 : 1;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Config
     */
    public function setId(int $id): Config
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Laila
     */
    public function getLaila(): Laila
    {
        return $this->laila;
    }

    /**
     * @param Laila $laila
     * @return Config
     */
    public function setLaila(Laila $laila): Config
    {
        $this->laila = $laila;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRunning(): bool
    {
        return $this->running;
    }

    /**
     * @param bool $running
     * @return Config
     */
    public function setRunning(bool $running): Config
    {
        $this->running = $running;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOpenNewOrders(): bool
    {
        return $this->openNewOrders;
    }

    /**
     * @param bool $openNewOrders
     * @return Config
     */
    public function setOpenNewOrders(bool $openNewOrders): Config
    {
        $this->openNewOrders = $openNewOrders;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDemoMode(): bool
    {
        return $this->demoMode;
    }

    /**
     * @param bool $demoMode
     * @return Config
     */
    public function setDemoMode(bool $demoMode): Config
    {
        $this->demoMode = $demoMode;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrend(): int
    {
        return $this->trend;
    }

    /**
     * @param int $trend
     * @return Config
     */
    public function setTrend(int $trend): Config
    {
        $this->trend = $trend;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnalyzeInterval(): string
    {
        return $this->analyzeInterval;
    }

    /**
     * @param string $analyzeInterval
     * @return Config
     */
    public function setAnalyzeInterval(string $analyzeInterval): Config
    {
        $this->analyzeInterval = $analyzeInterval;

        return $this;
    }

    /**
     * @return int
     */
    public function getAnalyzeTicks(): int
    {
        return $this->analyzeTicks;
    }

    /**
     * @param int $analyzeTicks
     * @return Config
     */
    public function setAnalyzeTicks(int $analyzeTicks): Config
    {
        $this->analyzeTicks = $analyzeTicks;

        return $this;
    }

    /**
     * @return float
     */
    public function getProtectedZoneUp(): float
    {
        return $this->protectedZoneUp;
    }

    /**
     * @param float $protectedZoneUp
     * @return Config
     */
    public function setProtectedZoneUp(float $protectedZoneUp): Config
    {
        $this->protectedZoneUp = $protectedZoneUp;

        return $this;
    }

    /**
     * @return float
     */
    public function getProtectedZoneDown(): float
    {
        return $this->protectedZoneDown;
    }

    /**
     * @param float $protectedZoneDown
     * @return Config
     */
    public function setProtectedZoneDown(float $protectedZoneDown): Config
    {
        $this->protectedZoneDown = $protectedZoneDown;

        return $this;
    }

    /**
     * @return float
     */
    public function getOrderAmount(): float
    {
        return $this->orderAmount;
    }

    /**
     * @param float $orderAmount
     * @return Config
     */
    public function setOrderAmount(float $orderAmount): Config
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * @return bool
     */
    public function isUseMinOrderAmount(): bool
    {
        return $this->useMinOrderAmount;
    }

    /**
     * @param bool $useMinOrderAmount
     * @return Config
     */
    public function setUseMinOrderAmount(bool $useMinOrderAmount): Config
    {
        $this->useMinOrderAmount = $useMinOrderAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getFees(): float
    {
        return $this->fees;
    }

    /**
     * @param float $fees
     * @return Config
     */
    public function setFees(float $fees): Config
    {
        $this->fees = $fees;

        return $this;
    }

    /**
     * @return float
     */
    public function getTakeProfitPercentage(): float
    {
        return $this->takeProfitPercentage;
    }

    /**
     * @param float $takeProfitPercentage
     * @return Config
     */
    public function setTakeProfitPercentage(float $takeProfitPercentage): Config
    {
        $this->takeProfitPercentage = $takeProfitPercentage;

        return $this;
    }

    /**
     * @return int
     */
    public function getActiveOrderCount(): int
    {
        return $this->activeOrderCount;
    }

    /**
     * @param int $activeOrderCount
     * @return Config
     */
    public function setActiveOrderCount(int $activeOrderCount): Config
    {
        $this->activeOrderCount = $activeOrderCount;

        return $this;
    }

    /**
     * @return float
     */
    public function getNewOrderShiftPercentage(): float
    {
        return $this->newOrderShiftPercentage;
    }

    /**
     * @param float $newOrderShiftPercentage
     * @return Config
     */
    public function setNewOrderShiftPercentage(float $newOrderShiftPercentage): Config
    {
        $this->newOrderShiftPercentage = $newOrderShiftPercentage;

        return $this;
    }

    /**
     * @return int
     */
    public function getNewOrderMaxWaitTimeToShift(): int
    {
        return $this->newOrderMaxWaitTimeToShift;
    }

    /**
     * @param int $newOrderMaxWaitTimeToShift
     * @return Config
     */
    public function setNewOrderMaxWaitTimeToShift(int $newOrderMaxWaitTimeToShift): Config
    {
        $this->newOrderMaxWaitTimeToShift = $newOrderMaxWaitTimeToShift;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderAmountPrecision(): int
    {
        return $this->orderAmountPrecision;
    }

    /**
     * @param int $orderAmountPrecision
     * @return Config
     */
    public function setOrderAmountPrecision(int $orderAmountPrecision): Config
    {
        $this->orderAmountPrecision = $orderAmountPrecision;

        return $this;
    }

    /**
     * @return bool
     */
    public function isClaimLeftSide(): bool
    {
        return $this->claimLeftSide;
    }

    /**
     * @param bool $claimLeftSide
     * @return Config
     */
    public function setClaimLeftSide(bool $claimLeftSide): Config
    {
        $this->claimLeftSide = $claimLeftSide;

        return $this;
    }

    /**
     * @return int
     */
    public function getPricePrecision(): int
    {
        return $this->pricePrecision;
    }

    /**
     * @param int $pricePrecision
     * @return Config
     */
    public function setPricePrecision(int $pricePrecision): Config
    {
        $this->pricePrecision = $pricePrecision;

        return $this;
    }
}
