<?php declare(strict_types=1);

namespace App\Entity;

use JMS\Serializer\Annotation as JMS;

final class Market
{
    /**
     * @var string
     */
    private $symbol;

    /**
     * @var boolean
     */
    private $open;

    /**
     * @var string
     */
    private $openMsg;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var int
     */
    private $fearAndGreed;

    /**
     * @var string
     */
    private $fearAndGreedName;

    /**
     * @var int
     */
    private $fearAndGreedChange;

    /**
     * @var PrevDay
     */
    private $prevDay;

    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $maxBuyPrice;

    /**
     * @var float
     */
    private $minSellPrice;

    /**
     * @var CandleTickData
     */
    private $candleTickData;

    /**
     * @var Laila
     * @JMS\Exclude()
     */
    private $laila;

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return Market
     */
    public function setSymbol(string $symbol): Market
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        return $this->open;
    }

    /**
     * @param bool $open
     * @return Market
     */
    public function setOpen(bool $open): Market
    {
        $this->open = $open;

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return Market
     */
    public function setOrder(Order $order): Market
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return int
     */
    public function getFearAndGreed(): int
    {
        return $this->fearAndGreed;
    }

    /**
     * @param int $fearAndGreed
     * @return Market
     */
    public function setFearAndGreed(int $fearAndGreed): Market
    {
        $this->fearAndGreed = $fearAndGreed;

        return $this;
    }

    /**
     * @return string
     */
    public function getFearAndGreedName(): string
    {
        return $this->fearAndGreedName;
    }

    /**
     * @param string $fearAndGreedName
     * @return Market
     */
    public function setFearAndGreedName(string $fearAndGreedName): Market
    {
        $this->fearAndGreedName = $fearAndGreedName;

        return $this;
    }

    /**
     * @return int
     */
    public function getFearAndGreedChange(): int
    {
        return $this->fearAndGreedChange;
    }

    /**
     * @param int $fearAndGreedChange
     * @return Market
     */
    public function setFearAndGreedChange(int $fearAndGreedChange): Market
    {
        $this->fearAndGreedChange = $fearAndGreedChange;

        return $this;
    }

    /**
     * @return PrevDay
     */
    public function getPrevDay(): PrevDay
    {
        return $this->prevDay;
    }

    /**
     * @param PrevDay $prevDay
     * @return Market
     */
    public function setPrevDay(PrevDay $prevDay): Market
    {
        $this->prevDay = $prevDay;

        return $this;
    }

    /**
     * @return float
     */
    public function getMaxBuyPrice(): float
    {
        return $this->maxBuyPrice;
    }

    /**
     * @param float $maxBuyPrice
     * @return Market
     */
    public function setMaxBuyPrice(float $maxBuyPrice): Market
    {
        $this->maxBuyPrice = $maxBuyPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getMinSellPrice(): float
    {
        return $this->minSellPrice;
    }

    /**
     * @param float $minSellPrice
     * @return Market
     */
    public function setMinSellPrice(float $minSellPrice): Market
    {
        $this->minSellPrice = $minSellPrice;

        return $this;
    }

    /**
     * @return CandleTickData
     */
    public function getCandleTickData(): CandleTickData
    {
        return $this->candleTickData;
    }

    /**
     * @param CandleTickData $candleTickData
     * @return Market
     */
    public function setCandleTickData(CandleTickData $candleTickData): Market
    {
        $this->candleTickData = $candleTickData;

        return $this;
    }

    /**
     * @return Laila
     */
    public function getLaila(): Laila
    {
        return $this->laila;
    }

    /**
     * @param Laila $laila
     * @return Market
     */
    public function setLaila(Laila $laila): Market
    {
        $this->laila = $laila;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Market
     */
    public function setPrice(float $price): Market
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getOpenMsg(): string
    {
        return $this->openMsg;
    }

    /**
     * @param string $openMsg
     * @return Market
     */
    public function setOpenMsg(string $openMsg): Market
    {
        $this->openMsg = $openMsg;

        return $this;
    }
}
