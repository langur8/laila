<?php declare(strict_types=1);

namespace App\Entity;

final class MiniTickerData
{
    /**
     * @var MiniTicker[]
     */
    private $miniTickers = [];

    /**
     * @param MiniTicker $miniTicker
     * @return MiniTickerData
     */
    public function addMiniTicker(MiniTicker $miniTicker): MiniTickerData
    {
        $this->miniTickers[] = $miniTicker;

        return $this;
    }

    /**
     * @return MiniTicker[]
     */
    public function getMiniTickers(): array
    {
        return $this->miniTickers;
    }
}
