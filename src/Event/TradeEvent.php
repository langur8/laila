<?php declare(strict_types=1);

namespace App\Event;

use App\Entity\Trade;
use Symfony\Contracts\EventDispatcher\Event;

final class TradeEvent extends Event
{
    public const PLACED = 'trade.placed';
    public const FILLED = 'trade.filled';
    public const CANCELED = 'trade.canceled';

    /**
     * @var Trade
     */
    private $trade;

    /**
     * @param Trade $trade
     */
    public function __construct(Trade $trade)
    {
        $this->trade = $trade;
    }

    /**
     * @return Trade
     */
    public function getTrade(): Trade
    {
        return $this->trade;
    }
}
