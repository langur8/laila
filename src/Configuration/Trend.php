<?php declare(strict_types=1);

namespace App\Configuration;

final class Trend
{
    public const UP = 1;
    public const NEUTRAL = 0;
    public const DOWN = -1;
}
