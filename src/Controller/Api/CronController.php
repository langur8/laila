<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use App\Entity\Laila;
use App\Facade\LailaFacade;
use App\Service\OrderService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pub/cron")
 */
final class CronController extends BaseRestController
{

    /**
     * @Rest\Get("/check-all")
     * @param Request      $request
     * @param OrderService $orderService
     * @param LailaFacade  $lailaFacade
     * @return Response
     * @throws \Exception
     */
    public function checkAll(OrderService $orderService, LailaFacade $lailaFacade): Response
    {
        try {
            /** @var Laila $lailas */
            $lailas = $lailaFacade->findRunning();
            foreach ($lailas as $laila) {
                $orderService->checkPositions($laila);
            }

            return $this->handleView($this->view(['check' => count($lailas) . ' ok']));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }
}
