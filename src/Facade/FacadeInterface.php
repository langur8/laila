<?php

namespace App\Facade;

interface FacadeInterface
{
    /**
     * @return string
     */
    public function getEntityName();

    /**
     * @param int $id
     * @return object
     */
    public function findOrCreateEntity($id);

    /**
     * @param int $id
     * @return object
     */
    public function find($id);

    /**
     * @param array|null $order
     * @return object[]
     */
    public function findAll(?array $order = null);

    /**
     * @param array|null $criteria
     * @return object
     */
    public function findOneBy(?array $criteria = null);

    /**
     * @param object $entity
     * @return void
     */
    public function save($entity);
}
