<?php declare(strict_types=1);

namespace App\Entity;

use App\Configuration\TradeStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="laila_order")
 */
class Order
{
    use TimestampableTrait;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $demoMode;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $side;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $symbol;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $openPrice;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $targetPrice;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $openAmount;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $targetAmount;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $closeAmount;

    /**
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    private $closePrice;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $profit;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $profitCurrency;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    private $fees;

    /**
     * @var Trade[]|Collection
     * @ORM\OneToMany(targetEntity="Trade", mappedBy="order", cascade={"persist", "remove"})
     */
    private $trades;

    /**
     * @var Laila
     * @ORM\ManyToOne(targetEntity="Laila", inversedBy="orders")
     * @ORM\JoinColumn(name="laila_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $laila;

    /**
     * @param Trade $trade
     * @return Order
     */
    public function addTrade(Trade $trade): Order
    {
        if ($this->trades->contains($trade) === false) {
            $this->trades->add($trade);
        }

        return $this;
    }


    /**
     * Order constructor
     */
    public function __construct()
    {
        $this->trades = new ArrayCollection();
    }

    /**
     * @param bool $closing
     * @return Trade|null
     */
    public function getActiveTrade(bool $closing): ?Trade
    {
        foreach ($this->getTrades() as $trade) {
            if ($trade->isClosing() === $closing && TradeStatus::isOpen($trade->getStatus())) {
                return $trade;
            }
        }

        return null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function setId(int $id): Order
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDemoMode(): bool
    {
        return $this->demoMode;
    }

    /**
     * @param bool $demoMode
     * @return Order
     */
    public function setDemoMode(bool $demoMode): Order
    {
        $this->demoMode = $demoMode;

        return $this;
    }

    /**
     * @return string
     */
    public function getSide(): string
    {
        return $this->side;
    }

    /**
     * @param string $side
     * @return Order
     */
    public function setSide(string $side): Order
    {
        $this->side = $side;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Order
     */
    public function setStatus(int $status): Order
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return Order
     */
    public function setSymbol(string $symbol): Order
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return float
     */
    public function getOpenPrice(): float
    {
        return $this->openPrice;
    }

    /**
     * @param float $openPrice
     * @return Order
     */
    public function setOpenPrice(float $openPrice): Order
    {
        $this->openPrice = $openPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getTargetPrice(): float
    {
        return $this->targetPrice;
    }

    /**
     * @param float $targetPrice
     * @return Order
     */
    public function setTargetPrice(float $targetPrice): Order
    {
        $this->targetPrice = $targetPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getOpenAmount(): float
    {
        return $this->openAmount;
    }

    /**
     * @param float $openAmount
     * @return Order
     */
    public function setOpenAmount(float $openAmount): Order
    {
        $this->openAmount = $openAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getTargetAmount(): float
    {
        return $this->targetAmount;
    }

    /**
     * @param float $targetAmount
     * @return Order
     */
    public function setTargetAmount(float $targetAmount): Order
    {
        $this->targetAmount = $targetAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getCloseAmount(): float
    {
        return $this->closeAmount;
    }

    /**
     * @param float $closeAmount
     * @return Order
     */
    public function setCloseAmount(float $closeAmount): Order
    {
        $this->closeAmount = $closeAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getClosePrice(): float
    {
        return $this->closePrice;
    }

    /**
     * @param float $closePrice
     * @return Order
     */
    public function setClosePrice(float $closePrice): Order
    {
        $this->closePrice = $closePrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getProfit(): float
    {
        return $this->profit;
    }

    /**
     * @param float $profit
     * @return Order
     */
    public function setProfit(float $profit): Order
    {
        $this->profit = $profit;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfitCurrency(): string
    {
        return $this->profitCurrency;
    }

    /**
     * @param string $profitCurrency
     * @return Order
     */
    public function setProfitCurrency(string $profitCurrency): Order
    {
        $this->profitCurrency = $profitCurrency;

        return $this;
    }

    /**
     * @return float
     */
    public function getFees(): float
    {
        return $this->fees;
    }

    /**
     * @param float $fees
     * @return Order
     */
    public function setFees(float $fees): Order
    {
        $this->fees = $fees;

        return $this;
    }

    /**
     * @return Trade[]|Collection
     */
    public function getTrades()
    {
        return $this->trades;
    }

    /**
     * @param Trade[]|Collection $trades
     * @return Order
     */
    public function setTrades($trades)
    {
        $this->trades = $trades;

        return $this;
    }

    /**
     * @return Laila
     */
    public function getLaila(): Laila
    {
        return $this->laila;
    }

    /**
     * @param Laila $laila
     * @return Order
     */
    public function setLaila(Laila $laila): Order
    {
        $this->laila = $laila;

        return $this;
    }
}
