<?php declare(strict_types=1);

namespace App\Facade;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ObjectRepository;

abstract class FacadeAbstract implements FacadeInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var string
     */
    protected $entityName;

    /**
     * @return string
     */
    public function getEntityName()
    {
        return $this->entityName;
    }

    /**
     * @param int $id
     * @return object
     */
    public function findOrCreateEntity($id)
    {
        if ($id === null) {
            return new $this->entityName;
        }
        $entity = $this->find($id);
        if (!$entity instanceof $this->entityName) {
            throw new \InvalidArgumentException('Invalid entity ID');
        }

        return $entity;
    }

    /**
     * @param mixed $id
     * @return null|object
     */
    public function find($id)
    {
        return $this->em->getRepository($this->getEntityName())->find($id);
    }

    /**
     * @param array|null $order
     * @return object[]
     */
    public function findAll(?array $order = null): array
    {
        return $this->em->getRepository($this->getEntityName())->findBy([], $order);
    }

    /**
     * @param array|null $criteria
     * @return null|object
     */
    public function findOneBy(?array $criteria = null)
    {
        return $this->em->getRepository($this->getEntityName())->findOneBy($criteria);
    }

    /**
     * @param array|null $criteria
     * @param array|null $order
     * @param int|null   $limit
     * @return object[]
     */
    public function findBy(?array $criteria = null, ?array $order = null, ?int $limit = null)
    {
        return $this->em->getRepository($this->getEntityName())->findBy($criteria, $order, $limit);
    }

    /**
     * @param object $entity
     */
    public function delete($entity): void
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * @param object $entity
     */
    public function save($entity)
    {
        if (!$entity instanceof $this->entityName) {
            throw new \InvalidArgumentException('Try to save an Entity that does not match the Facade.');
        }
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @param array       $conditions
     * @param int         $page
     * @param int         $perPage
     * @param string      $sortBy
     * @param bool        $sortDesc
     * @param string|null $filter
     * @param array|null  $filterFields
     * @param array|null  $defaultOrder
     * @return mixed
     */
    public function findFilteredAndPaginated(
        int $page,
        int $perPage,
        string $sortBy,
        bool $sortDesc,
        ?array $conditions = null,
        ?string $filter = null,
        ?array $filterFields = null,
        ?array $defaultOrder = null
    ) {
        $qb = $this->getRepository()->createQueryBuilder('e');
        $this->conditions($qb, $conditions);
        if ($sortBy !== '') {
            $qb->addOrderBy('e.' . $sortBy, $sortDesc === true ? 'DESC' : 'ASC');
        }
        if ($defaultOrder !== null) {
            foreach ($defaultOrder as $value => $dir) {
                $qb->addOrderBy('e.' . $value, $dir);
            }
        }
        $this->filter($qb, $filter, $filterFields);
        $paginator = new Paginator($qb);

        return $paginator->getQuery()
            ->setFirstResult(($page * $perPage) - $perPage)
            ->setMaxResults($perPage)
            ->getResult();
    }

    /**
     * @param string|null $filter
     * @param array|null  $filterFields
     * @param array|null  $conditions
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function findFilteredCount(
        ?string $filter = null,
        ?array $filterFields = null,
        ?array $conditions = null
    ) {
        $qb = $this->getRepository()->createQueryBuilder('e');
        $qb->select('count(e.id)');
        $this->conditions($qb, $conditions);
        $this->filter($qb, $filter, $filterFields);

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param object $entity
     */
    public function refresh($entity)
    {
        $this->em->refresh($entity);
    }

    /**
     * @return EntityRepository|ObjectRepository
     */
    protected function getRepository(): ObjectRepository
    {
        return $this->em->getRepository($this->getEntityName());
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @param QueryBuilder $qb
     * @param string|null  $filter
     * @param array|null   $filterFields
     */
    private function filter(QueryBuilder $qb, ?string $filter = null, ?array $filterFields = null): void
    {
        if ($filterFields === null || empty($filter)) {
            return;
        }
        $orx = $qb->expr()->orX();
        foreach ($filterFields as $field) {
            $orx->add($qb->expr()->like('e.' . $field, ':filter'));
        }
        $qb->andWhere($orx);
        $qb->setParameter('filter', '%' . $filter . '%');
    }

    /**
     * @param QueryBuilder $qb
     * @param array|null   $conditions
     */
    private function conditions(QueryBuilder $qb, ?array $conditions): void
    {
        if ($conditions === null) {
            return;
        }
        foreach ($conditions as $field => $value) {
            if ($value === null) {
                $qb->andWhere('e.' . $field . ' IS NULL');
                continue;
            }
            $qb->andWhere('e.' . $field . ' = :' . $field . 'Param');
            $qb->setParameter($field . 'Param', $value);
        }
    }
}
