<?php declare(strict_types=1);

namespace App\Entity;

final class FearAndGreedIndexData
{
    /**
     * @var FearAndGreedIndex[]
     */
    private $data;

    /**
     * @return FearAndGreedIndex|null
     */
    public function getCurrent(): ?FearAndGreedIndex
    {
        if (count($this->data) === 0) {
            return null;
        }

        return $this->data[0];
    }

    /**
     * @return int|null
     */
    public function getLastShift(): ?int
    {
        if (count($this->data) < 2) {
            return null;
        }

        return $this->data[0]->getValue() - $this->data[1]->getValue();
    }

    /**
     * @param FearAndGreedIndex $fearAndGreedIndex
     * @return FearAndGreedIndexData
     */
    public function addData(FearAndGreedIndex $fearAndGreedIndex): FearAndGreedIndexData
    {
        $this->data[] = $fearAndGreedIndex;

        return $this;
    }

    /**
     * @return FearAndGreedIndex[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param FearAndGreedIndex[] $data
     * @return FearAndGreedIndexData
     */
    public function setData(array $data): FearAndGreedIndexData
    {
        $this->data = $data;

        return $this;
    }
}
