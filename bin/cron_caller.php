<?php
// Request configuration, please fill these
$config = [
    'base_domain'    => 'e-skipass.cz', // webhosting domain
    'request_domain' => 'laila-be.e-skipass.cz', // domain or subdomain name - directory name on webserver
    'request_scheme' => 'https', // request protocol - http or https
    'script_name'    => '/bin/cron', // existing target script name (no rewrite or redirect!), including path (when in subdirectory)
    'query_string'   => '', // request variables
];

// Request execution, nothing needs to be changed down here
$_SERVER['SCRIPT_NAME'] = $config['script_name'];
$_SERVER['REQUEST_URI'] = $config['query_string'] != '' ? $config['script_name'] . '?' . $config['query_string'] : $config['script_name'];
$_SERVER['QUERY_STRING'] = $config['query_string'];
$_SERVER['REQUEST_METHOD'] = 'GET';
$_SERVER['REMOTE_PORT'] = 10000;
$_SERVER['SCRIPT_FILENAME'] = '/home/www/' . $config['base_domain'] . '/www/' . $config['request_domain'] . '/master' . $config['script_name'];
//$_SERVER['SCRIPT_FILENAME'] = '/var/www/html/eskipass' . $config['script_name'];
$_SERVER['CONTEXT_DOCUMENT_ROOT'] = '/home/www/' . $config['base_domain'] . '/www/' . $config['request_domain'] . '/';
$_SERVER['DOCUMENT_ROOT'] = $_SERVER['CONTEXT_DOCUMENT_ROOT'];
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_SERVER['SERVER_ADDR'] = '127.0.0.1';
$_SERVER['SERVER_NAME'] = $config['request_domain'];
$_SERVER['HTTP_HOST'] = $config['request_domain'];
$_SERVER['SERVER_SOFTWARE'] = 'Apache/2.4.10 (Debian)';
$_SERVER['PHP_SELF'] = $config['script_name'];
$_SERVER['REQUEST_SCHEME'] = $config['request_scheme'];

if ($config['query_string']) {
    foreach (explode('&', $config['query_string']) as $query_item) {
        [$key, $value] = explode('=', $query_item);
        $_GET[$key] = $value;
    }
}

chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require $_SERVER['SCRIPT_FILENAME'];
