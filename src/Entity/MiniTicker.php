<?php declare(strict_types=1);

namespace App\Entity;

final class MiniTicker
{
    /**
     * @var string
     */
    private $symbol;

    /**
     * @var float
     */
    private $close;

    /**
     * @var float
     */
    private $open;

    /**
     * @var float
     */
    private $high;

    /**
     * @var float
     */
    private $low;

    /**
     * @var float
     */
    private $volume;

    /**
     * @var float
     */
    private $quoteVolume;

    /**
     * @var int
     */
    private $eventTime;

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return MiniTicker
     */
    public function setSymbol(string $symbol): MiniTicker
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return float
     */
    public function getClose(): float
    {
        return $this->close;
    }

    /**
     * @param float $close
     * @return MiniTicker
     */
    public function setClose(float $close): MiniTicker
    {
        $this->close = $close;

        return $this;
    }

    /**
     * @return float
     */
    public function getOpen(): float
    {
        return $this->open;
    }

    /**
     * @param float $open
     * @return MiniTicker
     */
    public function setOpen(float $open): MiniTicker
    {
        $this->open = $open;

        return $this;
    }

    /**
     * @return float
     */
    public function getHigh(): float
    {
        return $this->high;
    }

    /**
     * @param float $high
     * @return MiniTicker
     */
    public function setHigh(float $high): MiniTicker
    {
        $this->high = $high;

        return $this;
    }

    /**
     * @return float
     */
    public function getLow(): float
    {
        return $this->low;
    }

    /**
     * @param float $low
     * @return MiniTicker
     */
    public function setLow(float $low): MiniTicker
    {
        $this->low = $low;

        return $this;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * @param float $volume
     * @return MiniTicker
     */
    public function setVolume(float $volume): MiniTicker
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return float
     */
    public function getQuoteVolume(): float
    {
        return $this->quoteVolume;
    }

    /**
     * @param float $quoteVolume
     * @return MiniTicker
     */
    public function setQuoteVolume(float $quoteVolume): MiniTicker
    {
        $this->quoteVolume = $quoteVolume;

        return $this;
    }

    /**
     * @return int
     */
    public function getEventTime(): int
    {
        return $this->eventTime;
    }

    /**
     * @param int $eventTime
     * @return MiniTicker
     */
    public function setEventTime(int $eventTime): MiniTicker
    {
        $this->eventTime = $eventTime;

        return $this;
    }
}
