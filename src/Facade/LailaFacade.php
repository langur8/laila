<?php declare(strict_types=1);

namespace App\Facade;

use App\Entity\Laila;
use Doctrine\ORM\EntityManagerInterface;

final class LailaFacade extends FacadeAbstract
{
    /**
     * @var string
     */
    protected $entityName = Laila::class;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Laila $entity
     * @throws \Exception
     */
    public function delete($entity): void
    {
        $entity->setDeletedAt(new \DateTime());
        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @return Laila[]
     */
    public function findRunning(): array
    {
        return $this->getRepository()->createQueryBuilder('l')
            ->leftJoin('l.config', 'c')
            ->where('l.deletedAt IS NULL')
            ->andWhere('c.running = :true')
            ->setParameter('true', true)
            ->getQuery()
            ->getResult();
    }
}
