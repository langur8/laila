<?php declare(strict_types=1);

namespace App\Configuration;

final class TradeSide
{
    public const BUY = 'BUY';
    public const SELL = 'SELL';
}
