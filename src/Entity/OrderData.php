<?php declare(strict_types=1);

namespace App\Entity;

use App\Configuration\OrderStatus;
use JMS\Serializer\Annotation as JMS;

final class OrderData
{
    /**
     * @var Order[]
     * @JMS\Exclude()
     */
    private $orders;

    /**
     * @var Laila
     */
    private $laila;

    /**
     * OrderData constructor.
     * @param array $orders
     * @param Laila $laila
     */
    public function __construct(array $orders, Laila $laila)
    {
        $this->orders = $orders;
        $this->laila = $laila;
    }

    /**
     * @return string
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("baseAsset")
     */
    public function getBaseAsset(): string
    {
        return $this->laila->getBaseAsset();
    }

    /**
     * @return float
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("baseAssetProfit")
     */
    public function getBaseAssetProfit(): float
    {
        $profit = 0;
        foreach ($this->orders as $order) {
            if ($order->getStatus() === OrderStatus::CLOSED && $order->getProfitCurrency() === $this->getBaseAsset()) {
                $profit += $order->getProfit();
            }
        }

        return $profit;
    }

    /**
     * @return Order[]
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("orders")
     */
    public function getOpenOrders(): array
    {
        $orders = [];
        foreach ($this->orders as $order) {
            if (in_array($order->getStatus(), OrderStatus::SYNC_STATUS)) {
                $orders[] = $order;
            }
        }

        return $orders;
    }

    /**
     * @return string
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("quoteAsset")
     */
    public function getQuoteAsset()
    {
        return $this->laila->getQuoteAsset();
    }

    /**
     * @return float
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("quoteAssetProfit")
     */
    public function getQuoteAssetProfit(): float
    {
        $profit = 0;
        foreach ($this->orders as $order) {
            if ($order->getStatus() === OrderStatus::CLOSED && $order->getProfitCurrency() === $this->getQuoteAsset()) {
                $profit += $order->getProfit();
            }
        }

        return $profit;
    }

    /**
     * @return float
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("fees")
     */
    public function getFees(): float
    {
        $fees = 0;
        foreach ($this->orders as $order) {
            if ($order->getStatus() === OrderStatus::CLOSED) {
                $fees += $order->getFees();
            }
        }

        return $fees;
    }

    /**
     * @return int
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("openOrders")
     */
    public function countOpenOrders(): int
    {
        $count = 0;
        foreach ($this->orders as $order) {
            if (in_array($order->getStatus(), OrderStatus::ACTIVE_STATUS)) {
                $count += 1;
            }
        }

        return $count;
    }

    /**
     * @return int|void
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("total")
     */
    public function countOrders()
    {
        return count($this->orders);
    }
}
