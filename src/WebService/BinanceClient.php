<?php declare(strict_types=1);


namespace App\WebService;

use App\Configuration\TradeStatus;
use App\Entity\BookPrice;
use App\Entity\CandleTick;
use App\Entity\CandleTickData;
use App\Entity\ExchangeInfo;
use App\Entity\PrevDay;
use App\Entity\Trade;
use App\Exception\BinanceResponseException;
use Binance\API;

final class BinanceClient implements TradeClientInterface
{
    /**
     * @var API
     */
    private $api;

    /**
     * @param string $binanceKey
     * @param string $binanceSecret
     */
    public function __construct(string $binanceKey, string $binanceSecret)
    {
        $this->api = new API($binanceKey, $binanceSecret);
        $this->api->caOverride = true;
    }

    /**
     * @param string $symbol
     * @return float
     * @throws \Exception
     */
    public function getPrice(string $symbol): float
    {
        return (float) $this->api->price($symbol);
    }

    /**
     * @return string[]
     * @throws \Exception
     */
    public function getPrices(): array
    {
        return $this->api->prices();
    }

    /**
     * @param string $symbol
     * @return PrevDay
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getPrevDay(string $symbol): PrevDay
    {
        $result = $this->api->prevDay($symbol);
        $prevDay = new PrevDay();
        ArrayToEntityMapper::mapToEntity($result, $prevDay);

        return $prevDay;
    }

    /**
     * @param string $market
     * @param string $interval
     * @param int    $limit
     * @return CandleTickData
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getCandleStickData(string $market, string $interval, int $limit = 60): CandleTickData
    {
        $ticks = $this->api->candlesticks($market, $interval, $limit);

        $data = new CandleTickData();
        $data->setMarketName($market)->setInterval($interval);

        foreach ($ticks as $tick) {
            $candleTick = new CandleTick();
            ArrayToEntityMapper::mapToEntity($tick, $candleTick);
            $data->addCandleTick($candleTick);
        }

        return $data;
    }

    /**
     * @param string $symbol
     * @return ExchangeInfo
     * @throws \Exception
     */
    public function exchangeInfo(string $symbol): ExchangeInfo
    {
        $data = $this->api->exchangeInfo();

        $info = new ExchangeInfo();
        ArrayToEntityMapper::mapToEntity(isset($data['symbols'][$symbol]) ? $data['symbols'][$symbol] : [], $info);

        return $info;
    }

    /**
     * @param string $symbol
     * @return array
     * @throws \Exception
     */
    public function depth(string $symbol): array
    {
        return $this->api->depth($symbol);
    }

    /**
     * @param string $symbol
     * @return BookPrice
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function bookPrice(string $symbol): BookPrice
    {
        $data = $this->api->bookPrices();
        $bookPrice = new BookPrice;
        ArrayToEntityMapper::mapToEntity(isset($data[$symbol]) ? $data[$symbol] : [], $bookPrice);

        return $bookPrice;
    }

    /**
     * @param Trade $trade
     * @param array $flags
     * @param bool  $test
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function order(Trade $trade, array $flags = [], bool $test = false): void
    {
        $order = $this->api->order(
            $trade->getSide(),
            $trade->getSymbol(),
            $trade->getOrigQty(),
            $trade->getPrice(),
            $trade->getType(),
            $flags,
            $test
        );
        $this->checkResponse($order);
        if ($test === true) {
            $trade
                ->setStatus(TradeStatus::NEW)
                ->setTransactTime(time() * 1000);
            return;
        }
        ArrayToEntityMapper::mapToEntity($order, $trade);
    }

    /**
     * @param Trade $trade
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getOrderStatus(Trade $trade): void
    {
        if ($trade->getOrder()->isDemoMode()) {
            return;
        }
        ArrayToEntityMapper::mapToEntity(
            $this->api->orderStatus($trade->getSymbol(),
                $trade->getBinanceOrderId()), $trade
        );
    }

    /**
     * @param Trade $trade
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function cancel(Trade $trade): void
    {
        if ($trade->getOrder()->isDemoMode()) {
            return;
        }
        ArrayToEntityMapper::mapToEntity(
            $this->api->cancel($trade->getSymbol(),
                $trade->getBinanceOrderId()), $trade
        );
    }

    /**
     * @param array $data
     */
    private function checkResponse(array $data): void
    {
        if (isset($data['code']) && $data['code'] <> 0) {
            throw new BinanceResponseException($data['msg'], $data['code']);
        }
    }
}
