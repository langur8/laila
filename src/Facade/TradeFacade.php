<?php declare(strict_types=1);

namespace App\Facade;

use App\Entity\Trade;
use Doctrine\ORM\EntityManagerInterface;

final class TradeFacade extends FacadeAbstract
{
    /**
     * @var string
     */
    protected $entityName = Trade::class;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}
