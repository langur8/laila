<?php declare(strict_types=1);

namespace App\Entity;

final class BookPrice
{
    /**
     * @var float
     */
    private $bid;

    /**
     * @var float
     */
    private $bids;

    /**
     * @var float
     */
    private $ask;

    /**
     * @var float
     */
    private $asks;

    /**
     * @return float
     */
    public function getBid(): float
    {
        return $this->bid;
    }

    /**
     * @param float $bid
     * @return BookPrice
     */
    public function setBid(float $bid): BookPrice
    {
        $this->bid = $bid;

        return $this;
    }

    /**
     * @return float
     */
    public function getBids(): float
    {
        return $this->bids;
    }

    /**
     * @param float $bids
     * @return BookPrice
     */
    public function setBids(float $bids): BookPrice
    {
        $this->bids = $bids;

        return $this;
    }

    /**
     * @return float
     */
    public function getAsk(): float
    {
        return $this->ask;
    }

    /**
     * @param float $ask
     * @return BookPrice
     */
    public function setAsk(float $ask): BookPrice
    {
        $this->ask = $ask;

        return $this;
    }

    /**
     * @return float
     */
    public function getAsks(): float
    {
        return $this->asks;
    }

    /**
     * @param float $asks
     * @return BookPrice
     */
    public function setAsks(float $asks): BookPrice
    {
        $this->asks = $asks;

        return $this;
    }
}
