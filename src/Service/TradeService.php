<?php declare(strict_types=1);


namespace App\Service;

use App\Configuration\TradeSide;
use App\Configuration\TradeStatus;
use App\Configuration\TradeType;
use App\Entity\Order;
use App\Entity\Trade;
use App\Event\TradeEvent;
use App\Facade\TradeFacade;
use App\WebService\BinanceClient;
use App\WebService\DemoTradeClient;
use App\WebService\TradeClientInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class TradeService
{
    /**
     * @var BinanceClient
     */
    private $binanceClient;

    /**
     * @var DemoTradeClient
     */
    private $demoTradeClient;

    /**
     * @var TradeFacade
     */
    private $tradeFacade;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * TradeService constructor.
     * @param BinanceClient            $binanceClient
     * @param DemoTradeClient          $demoTradeClient
     * @param TradeFacade              $tradeFacade
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        BinanceClient $binanceClient,
        DemoTradeClient $demoTradeClient,
        TradeFacade $tradeFacade,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->binanceClient = $binanceClient;
        $this->demoTradeClient = $demoTradeClient;
        $this->tradeFacade = $tradeFacade;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Order $order
     * @throws \ReflectionException
     */
    public function createOpeningTrade(Order $order): void
    {
        $trade = new Trade;
        $trade
            ->setSymbol($order->getSymbol())
            ->setPrice($order->getOpenPrice())
            ->setOrigQty($order->getOpenAmount())
            ->setSide($order->getSide())
            ->setOrder($order)
            ->setClosing(false)
            ->setType(TradeType::LIMIT);

        $this->binanceClient->order($trade, [], $order->isDemoMode());
        $this->tradeFacade->save($trade);

        $this->processState($trade);
    }

    /**
     * @param Order $order
     * @throws \ReflectionException
     */
    public function createClosingTrade(Order $order): void
    {
        $trade = new Trade;
        $trade
            ->setSymbol($order->getSymbol())
            ->setPrice($order->getTargetPrice())
            ->setOrigQty($order->getTargetAmount())
            ->setSide($order->getSide() === TradeSide::BUY ? TradeSide::SELL : TradeSide::BUY)
            ->setOrder($order)
            ->setClosing(true)
            ->setType(TradeType::LIMIT);

        $this->binanceClient->order($trade, [], $order->isDemoMode());
        $this->tradeFacade->save($trade);

        $this->processState($trade);
    }

    /**
     * @param Trade $trade
     * @throws \ReflectionException
     */
    public function syncTrade(Trade $trade): void
    {
        if (!TradeStatus::isOpen($trade->getStatus())) {
            return;
        }

        $currentState = $trade->getStatus();
        $this->getClient($trade->getOrder()->isDemoMode())->getOrderStatus($trade);
        $this->tradeFacade->save($trade);
        if ($currentState !== $trade->getStatus()) {
            $this->processState($trade);
        }
        //
    }

    /**
     * @param Trade $trade
     * @return Void
     */
    public function cancelTrade(Trade $trade): Void
    {
        $this->getClient($trade->getOrder()->isDemoMode())->cancel($trade);
        $this->tradeFacade->save($trade);
        $this->processState($trade);
    }

    /**
     * @param Trade $trade
     */
    private function processState(Trade $trade): void
    {
        $eventType = null;
        switch ($trade->getStatus()) {
            case TradeStatus::NEW :
                $eventType = TradeEvent::PLACED;
                break;
            case TradeStatus::FILLED :
                $eventType = TradeEvent::FILLED;
                break;

            case TradeStatus::EXPIRED:
            case TradeStatus::CANCELED:
            case TradeStatus::REJECTED:
                $eventType = TradeEvent::CANCELED;
        }

        if ($eventType === null) {
            return;
        }

        $event = new TradeEvent($trade);
        $this->eventDispatcher->dispatch($event, $eventType);
    }

    /**
     * @param bool $demo
     * @return TradeClientInterface
     */
    private function getClient(bool $demo): TradeClientInterface
    {
        return $demo === true ? $this->demoTradeClient : $this->binanceClient;
    }
}
