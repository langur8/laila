<?php declare(strict_types=1);

namespace App\Entity;

final class PrevDay
{
    /**
     * @var string
     */
    private $symbol;

    /**
     * @var float
     */
    private $priceChange;

    /**
     * @var float
     */
    private $priceChangePercent;

    /**
     * @var float
     */
    private $weightedAvgPrice;

    /**
     * @var float
     */
    private $lastPrice;

    /**
     * @var float
     */
    private $lastQty;

    /**
     * @var float
     */
    private $bidPrice;

    /**
     * @var float
     */
    private $bidQty;

    /**
     * @var float
     */
    private $askPrice;

    /**
     * @var float
     */
    private $askQty;

    /**
     * @var float
     */
    private $openPrice;

    /**
     * @var float
     */
    private $highPrice;

    /**
     * @var float
     */
    private $lowPrice;

    /**
     * @var float
     */
    private $volume;

    /**
     * @var float
     */
    private $quoteVolume;

    /**
     * @var int
     */
    private $openTime;

    /**
     * @var int
     */
    private $closeTime;

    /**
     * @var int
     */
    private $firstId;

    /**
     * @var int
     */
    private $lastId;

    /**
     * @var int
     */
    private $count;

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return PrevDay
     */
    public function setSymbol(string $symbol): PrevDay
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceChange(): float
    {
        return $this->priceChange;
    }

    /**
     * @param float $priceChange
     * @return PrevDay
     */
    public function setPriceChange(float $priceChange): PrevDay
    {
        $this->priceChange = $priceChange;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceChangePercent(): float
    {
        return $this->priceChangePercent;
    }

    /**
     * @param float $priceChangePercent
     * @return PrevDay
     */
    public function setPriceChangePercent(float $priceChangePercent): PrevDay
    {
        $this->priceChangePercent = $priceChangePercent;

        return $this;
    }

    /**
     * @return float
     */
    public function getWeightedAvgPrice(): float
    {
        return $this->weightedAvgPrice;
    }

    /**
     * @param float $weightedAvgPrice
     * @return PrevDay
     */
    public function setWeightedAvgPrice(float $weightedAvgPrice): PrevDay
    {
        $this->weightedAvgPrice = $weightedAvgPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getLastPrice(): float
    {
        return $this->lastPrice;
    }

    /**
     * @param float $lastPrice
     * @return PrevDay
     */
    public function setLastPrice(float $lastPrice): PrevDay
    {
        $this->lastPrice = $lastPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getLastQty(): float
    {
        return $this->lastQty;
    }

    /**
     * @param float $lastQty
     * @return PrevDay
     */
    public function setLastQty(float $lastQty): PrevDay
    {
        $this->lastQty = $lastQty;

        return $this;
    }

    /**
     * @return float
     */
    public function getBidPrice(): float
    {
        return $this->bidPrice;
    }

    /**
     * @param float $bidPrice
     * @return PrevDay
     */
    public function setBidPrice(float $bidPrice): PrevDay
    {
        $this->bidPrice = $bidPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getBidQty(): float
    {
        return $this->bidQty;
    }

    /**
     * @param float $bidQty
     * @return PrevDay
     */
    public function setBidQty(float $bidQty): PrevDay
    {
        $this->bidQty = $bidQty;

        return $this;
    }

    /**
     * @return float
     */
    public function getAskPrice(): float
    {
        return $this->askPrice;
    }

    /**
     * @param float $askPrice
     * @return PrevDay
     */
    public function setAskPrice(float $askPrice): PrevDay
    {
        $this->askPrice = $askPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getAskQty(): float
    {
        return $this->askQty;
    }

    /**
     * @param float $askQty
     * @return PrevDay
     */
    public function setAskQty(float $askQty): PrevDay
    {
        $this->askQty = $askQty;

        return $this;
    }

    /**
     * @return float
     */
    public function getOpenPrice(): float
    {
        return $this->openPrice;
    }

    /**
     * @param float $openPrice
     * @return PrevDay
     */
    public function setOpenPrice(float $openPrice): PrevDay
    {
        $this->openPrice = $openPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getHighPrice(): float
    {
        return $this->highPrice;
    }

    /**
     * @param float $highPrice
     * @return PrevDay
     */
    public function setHighPrice(float $highPrice): PrevDay
    {
        $this->highPrice = $highPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getLowPrice(): float
    {
        return $this->lowPrice;
    }

    /**
     * @param float $lowPrice
     * @return PrevDay
     */
    public function setLowPrice(float $lowPrice): PrevDay
    {
        $this->lowPrice = $lowPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * @param float $volume
     * @return PrevDay
     */
    public function setVolume(float $volume): PrevDay
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return float
     */
    public function getQuoteVolume(): float
    {
        return $this->quoteVolume;
    }

    /**
     * @param float $quoteVolume
     * @return PrevDay
     */
    public function setQuoteVolume(float $quoteVolume): PrevDay
    {
        $this->quoteVolume = $quoteVolume;

        return $this;
    }

    /**
     * @return int
     */
    public function getOpenTime(): int
    {
        return $this->openTime;
    }

    /**
     * @param int $openTime
     * @return PrevDay
     */
    public function setOpenTime(int $openTime): PrevDay
    {
        $this->openTime = $openTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getCloseTime(): int
    {
        return $this->closeTime;
    }

    /**
     * @param int $closeTime
     * @return PrevDay
     */
    public function setCloseTime(int $closeTime): PrevDay
    {
        $this->closeTime = $closeTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getFirstId(): int
    {
        return $this->firstId;
    }

    /**
     * @param int $firstId
     * @return PrevDay
     */
    public function setFirstId(int $firstId): PrevDay
    {
        $this->firstId = $firstId;

        return $this;
    }

    /**
     * @return int
     */
    public function getLastId(): int
    {
        return $this->lastId;
    }

    /**
     * @param int $lastId
     * @return PrevDay
     */
    public function setLastId(int $lastId): PrevDay
    {
        $this->lastId = $lastId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return PrevDay
     */
    public function setCount(int $count): PrevDay
    {
        $this->count = $count;

        return $this;
    }
}
