<?php declare(strict_types=1);

namespace App\Exception;

interface RestExceptionInterface
{
    /**
     * @return string|null
     */
    public function getClientMessage(): ?string;

    /**
     * @return array|null
     */
    public function getParams(): ?array;
}
