<?php declare(strict_types=1);


namespace App\Service;

use App\WebService\BinanceClient;

class BalanceService
{
    /**
     * @var BinanceClient
     */
    private $binanceClient;

    public function __construct(BinanceClient $binanceClient)
    {
        $this->binanceClient = $binanceClient;
    }

    /**
     * @throws \Exception
     */
    public function getPrices(): void
    {
        $this->binanceClient->getPrices();
    }
}
