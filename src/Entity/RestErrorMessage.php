<?php declare(strict_types=1);

namespace App\Entity;

final class RestErrorMessage
{
    /**
     * @var string
     */
    private $message;

    /**
     * @var string|null
     */
    private $error;

    /**
     * @var int|null
     */
    private $code;

    /**
     * @var array|null
     */
    private $validation;

    /**
     * @var array|null
     */
    private $params;

    /**
     * RestErrorMessage constructor.
     * @param string      $message
     * @param string|null $error
     * @param int         $code
     * @param array|null  $validation
     * @param array|null  $params
     */
    public function __construct(
        string $message,
        ?string $error = null,
        int $code = 0,
        ?array $validation = null,
        ?array $params = null
    ) {
        $this->message = $message;
        $this->error = $error;
        $this->code = $code;
        $this->validation = $validation;
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $return = [
            'error' =>
                [
                    'message' => $this->message,
                    'error'   => $this->error ?? $this->message,
                    'code'    => $this->code,
                ],
        ];
        if ($this->validation !== null) {
            $return['error']['validation'] = $this->validation;
        }
        if ($this->params !== null) {
            $return['error']['params'] = $this->params;
        }

        return $return;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return RestErrorMessage
     */
    public function setMessage(string $message): RestErrorMessage
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return RestErrorMessage
     */
    public function setCode(int $code): RestErrorMessage
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getValidation(): ?array
    {
        return $this->validation;
    }

    /**
     * @param array|null $validation
     * @return RestErrorMessage
     */
    public function setValidation(?array $validation): RestErrorMessage
    {
        $this->validation = $validation;

        return $this;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @param string $error
     * @return RestErrorMessage
     */
    public function setError(string $error): RestErrorMessage
    {
        $this->error = $error;

        return $this;
    }
}
