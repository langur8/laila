<?php declare(strict_types=1);

namespace App\WebService;

use App\Exception\CurlErrorException;
use App\Exception\JsonNotValidException;
use Symfony\Component\HttpFoundation\Response;

final class JsonService
{
    /**
     * Check if Json is valid
     * @param string $json
     * @return bool
     */
    public function isJsonValid(string $json): bool
    {
        json_decode($json);

        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * @param string      $url
     * @param array|null  $post
     * @param string|null $authToken
     * @return mixed
     */
    public function handleGetApiRequest(string $url, ?array $post = null, ?string $authToken = null)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, false);
        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $headers = ['Content-Type: application/json'];
        if ($authToken !== null) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $authToken]);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);

        if (curl_error($ch)) {
            throw new CurlErrorException('API returned error' . curl_error($ch), curl_errno($ch));
        }

        if ($this->isJsonValid($result)) {
            return json_decode($result, true);
        }

        throw new JsonNotValidException(
            'Returned JSON is not valid ' . $result . 'url: ' . $url, Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }
}
