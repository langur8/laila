<?php declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

final class RequestException extends RuntimeException implements RestExceptionInterface
{
    use RestExceptionTrait;

    /**
     * @var string
     */
    private $clientMessage = 'request_exception';
}
