<?php declare(strict_types=1);

namespace App\Facade;

use App\Configuration\OrderStatus;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;

final class OrderFacade extends FacadeAbstract
{
    /**
     * @var string
     */
    protected $entityName = Order::class;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $lailaId
     * @return Order[]
     */
    public function findOrdersToSync(int $lailaId): array
    {
        return $this->findBy(['status' => OrderStatus::SYNC_STATUS, 'laila' => $lailaId]);
    }

    /**
     * @param int $lailaId
     * @return int
     */
    public function countActiveOrders(int $lailaId): int
    {
        return count($this->findBy(['status' => OrderStatus::ACTIVE_STATUS, 'laila' => $lailaId]));
    }

    /**
     * @param int       $lailaId
     * @param \DateTime $createdTo
     * @return Order[]
     */
    public function findOldWaitingOrders(int $lailaId, \DateTime $createdTo): array
    {
        return $this->getRepository()->createQueryBuilder('o')
            ->where('o.laila = :lailaId' )
            ->andWhere('o.createdAt <= :createdTo')
            ->andWhere('o.status = :status')
            ->setParameter('lailaId', $lailaId)
            ->setParameter('createdTo', $createdTo)
            ->setParameter('status', OrderStatus::WAITING_OPEN)
            ->getQuery()
            ->getResult();
    }
}
