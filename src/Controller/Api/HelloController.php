<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class HelloController extends BaseRestController
{

    /**
     * @Rest\Get("/hello")
     * @param Request $request
     * @return JsonResponse
     */
    public function hello(Request $request): JsonResponse
    {
        return new JsonResponse(['data' => 'Hello from ' . $request->getUri()]);
    }

    /**
     * @Rest\Get("/error")
     * @return Response
     */
    public function error(): Response
    {
        return $this->exceptionResponse(new \LogicException('test'));
    }

}
