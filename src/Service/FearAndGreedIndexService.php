<?php declare(strict_types=1);

namespace App\Service;

use App\Entity\FearAndGreedIndex;
use App\Entity\FearAndGreedIndexData;
use App\Facade\FearAndGreedIndexFacade;
use App\WebService\ArrayToEntityMapper;
use App\WebService\JsonService;

final class FearAndGreedIndexService
{
    /**
     * @var JsonService
     */
    private $jsonService;

    /**
     * @var FearAndGreedIndexFacade
     */
    private $fearAndGreedIndexFacade;

    /**
     * @var string
     */
    private $endpoint = 'https://api.alternative.me/fng/?limit=';

    /**
     * FearAndGreedIndexService constructor.
     * @param JsonService             $jsonService
     * @param FearAndGreedIndexFacade $fearAndGreedIndexFacade
     */
    public function __construct(JsonService $jsonService, FearAndGreedIndexFacade $fearAndGreedIndexFacade)
    {
        $this->jsonService = $jsonService;
        $this->fearAndGreedIndexFacade = $fearAndGreedIndexFacade;
    }

    /**
     * @param int $limit
     * @return FearAndGreedIndexData
     * @throws \ReflectionException
     */
    public function getIndex(int $limit = 1): FearAndGreedIndexData
    {
        if ($limit < 1) {
            return new FearAndGreedIndexData();
        }
        $cached = $this->checkCache($limit);
        if ($cached instanceof FearAndGreedIndexData) {
            return $cached;
        }

        return $this->callApi($limit);
    }

    /**
     * @param int $limit
     * @return FearAndGreedIndexData|null
     */
    private function checkCache(int $limit): ?FearAndGreedIndexData
    {
        $return = new FearAndGreedIndexData();
        $cached = $this->fearAndGreedIndexFacade->findBy([], ['timestamp' => 'DESC'], $limit);
        if (count($cached) === $limit && $cached[0]->isCurrent() === true) {
            $return->setData($cached);

            return $return;
        }

        return null;
    }

    /**
     * @param int $limit
     * @return FearAndGreedIndexData
     * @throws \ReflectionException
     */
    private function callApi(int $limit): FearAndGreedIndexData
    {
        $return = new FearAndGreedIndexData();
        $result = $this->jsonService->handleGetApiRequest($this->endpoint . $limit);
        foreach ($result['data'] as $index) {
            $fearAndGreedIndex = new FearAndGreedIndex();
            ArrayToEntityMapper::mapToEntity($index, $fearAndGreedIndex, true);
            $return->addData($fearAndGreedIndex);
            if ($this->fearAndGreedIndexFacade->find($fearAndGreedIndex->getTimestamp()) === null) {
                $this->fearAndGreedIndexFacade->save($fearAndGreedIndex);
            }
        }

        return $return;
    }
}
