<?php declare(strict_types=1);

namespace App\Configuration;

final class OrderStatus
{
    public const CREATED = 0;
    public const WAITING_OPEN = 10;
    public const OPEN = 20;
    public const WAITING_CLOSE = 30;
    public const SLEEP = 40;
    public const CLOSED = -10;
    public const CANCELED = -20;

    public const SYNC_STATUS = [
        self::WAITING_OPEN,
        self::OPEN,
        self::WAITING_CLOSE,
        self::SLEEP,
    ];

    public const ACTIVE_STATUS = [
        self::WAITING_OPEN,
        self::OPEN,
        self::WAITING_CLOSE,
    ];
}
