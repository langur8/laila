<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use App\Entity\Laila;
use App\Entity\Order;
use App\Entity\OrderData;
use App\Facade\LailaFacade;
use App\Facade\OrderFacade;
use App\Service\MarketService;
use App\Service\OrderService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/order")
 */
final class OrderController extends BaseRestController
{

    /**
     * @Rest\Get("/check")
     * @param Request      $request
     * @param OrderService $orderService
     * @param LailaFacade  $lailaFacade
     * @return Response
     * @throws \Exception
     */
    public function check(Request $request, OrderService $orderService, LailaFacade $lailaFacade): Response
    {
        try {
            $id = $request->get('id', 0);
            if ($id === 0) {
                throw new \InvalidArgumentException('id');
            }
            $laila = $lailaFacade->find((int) $id);
            if (!$laila instanceof Laila) {
                throw new \InvalidArgumentException('not_found');
            }
            $orderService->checkPositions($laila);

            return $this->handleView($this->view(['check' => 'ok']));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Get("/list")
     * @param Request      $request
     * @param OrderService $orderService
     * @param LailaFacade  $lailaFacade
     * @return Response
     * @throws \Exception
     */
    public function lailaList(Request $request, OrderFacade $orderFacade, LailaFacade $lailaFacade): Response
    {
        try {
            $id = $request->get('id', 0);
            if ($id === 0) {
                throw new \InvalidArgumentException('id');
            }
            $laila = $lailaFacade->find((int) $id);
            if (!$laila instanceof Laila) {
                throw new \InvalidArgumentException('not_found');
            }

            return $this->handleView($this->view(new OrderData($orderFacade->findBy(['laila' => $laila], ['id' => 'DESC']), $laila)));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Post("")
     * @param Request      $request
     * @param OrderService $orderService
     * @param LailaFacade  $lailaFacade
     * @return Response
     */
    public function create(Request $request, OrderService $orderService, LailaFacade $lailaFacade, MarketService $marketService): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            if (empty($data['id'])) {
                throw new \InvalidArgumentException('id');
            }
            $laila = $lailaFacade->find((int) $data['id']);
            if (!$laila instanceof Laila) {
                throw new \InvalidArgumentException('not_found');
            }

            return $this->handleView($this->view($orderService->createOrder($marketService->create($laila))));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Post("/sleep")
     * @param Request      $request
     * @param OrderService $orderService
     * @param LailaFacade  $lailaFacade
     * @return Response
     * @throws \Exception
     */
    public function sleep(Request $request, OrderFacade $orderFacade, OrderService $orderService): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            if (empty($data['id'])) {
                throw new \InvalidArgumentException('id');
            }
            $order = $orderFacade->find((int) $data['id']);
            if (!$order instanceof Order) {
                throw new \InvalidArgumentException('not_found');
            }
            $orderService->sleepOrder($order);

            return $this->handleView($this->view(['check' => 'ok']));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

}
