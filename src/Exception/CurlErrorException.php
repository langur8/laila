<?php declare(strict_types=1);

namespace App\Exception;

final class CurlErrorException extends \RuntimeException implements RestExceptionInterface
{
    use RestExceptionTrait;

    /**
     * @var string
     */
    private $clientMessage = 'connection_exception';
}
