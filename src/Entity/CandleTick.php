<?php declare(strict_types=1);

namespace App\Entity;

final class CandleTick
{
    /**
     * @var float
     */
    private $open;

    /**
     * @var float
     */
    private $high;

    /**
     * @var float
     */
    private $low;

    /**
     * @var float
     */
    private $close;

    /**
     * @var float
     */
    private $volume;

    /**
     * @var int
     */
    private $openTime;

    /**
     * @var int
     */
    private $closeTime;

    /**
     * @var float
     */
    private $assetVolume;

    /**
     * @var float
     */
    private $baseVolume;

    /**
     * @var int
     */
    private $trades;

    /**
     * @var float
     */
    private $assetBuyVolume;

    /**
     * @var float
     */
    private $takerBuyVolume;

    /**
     * @var int
     */
    private $ignored;

    /**
     * @return bool
     */
    public function isUpTrend(): bool
    {
        return ($this->getHigh() - $this->getClose()) <  ($this->getClose() - $this->getLow());
    }

    /**
     * @return float
     */
    public function getOpen(): float
    {
        return $this->open;
    }

    /**
     * @param float $open
     * @return CandleTick
     */
    public function setOpen(float $open): CandleTick
    {
        $this->open = $open;

        return $this;
    }

    /**
     * @return float
     */
    public function getHigh(): float
    {
        return $this->high;
    }

    /**
     * @param float $high
     * @return CandleTick
     */
    public function setHigh(float $high): CandleTick
    {
        $this->high = $high;

        return $this;
    }

    /**
     * @return float
     */
    public function getLow(): float
    {
        return $this->low;
    }

    /**
     * @param float $low
     * @return CandleTick
     */
    public function setLow(float $low): CandleTick
    {
        $this->low = $low;

        return $this;
    }

    /**
     * @return float
     */
    public function getClose(): float
    {
        return $this->close;
    }

    /**
     * @param float $close
     * @return CandleTick
     */
    public function setClose(float $close): CandleTick
    {
        $this->close = $close;

        return $this;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return $this->volume;
    }

    /**
     * @param float $volume
     * @return CandleTick
     */
    public function setVolume(float $volume): CandleTick
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @return int
     */
    public function getOpenTime(): int
    {
        return $this->openTime;
    }

    /**
     * @param int $openTime
     * @return CandleTick
     */
    public function setOpenTime(int $openTime): CandleTick
    {
        $this->openTime = $openTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getCloseTime(): int
    {
        return $this->closeTime;
    }

    /**
     * @param int $closeTime
     * @return CandleTick
     */
    public function setCloseTime(int $closeTime): CandleTick
    {
        $this->closeTime = $closeTime;

        return $this;
    }

    /**
     * @return float
     */
    public function getAssetVolume(): float
    {
        return $this->assetVolume;
    }

    /**
     * @param float $assetVolume
     * @return CandleTick
     */
    public function setAssetVolume(float $assetVolume): CandleTick
    {
        $this->assetVolume = $assetVolume;

        return $this;
    }

    /**
     * @return float
     */
    public function getBaseVolume(): float
    {
        return $this->baseVolume;
    }

    /**
     * @param float $baseVolume
     * @return CandleTick
     */
    public function setBaseVolume(float $baseVolume): CandleTick
    {
        $this->baseVolume = $baseVolume;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrades(): int
    {
        return $this->trades;
    }

    /**
     * @param int $trades
     * @return CandleTick
     */
    public function setTrades(int $trades): CandleTick
    {
        $this->trades = $trades;

        return $this;
    }

    /**
     * @return float
     */
    public function getAssetBuyVolume(): float
    {
        return $this->assetBuyVolume;
    }

    /**
     * @param float $assetBuyVolume
     * @return CandleTick
     */
    public function setAssetBuyVolume(float $assetBuyVolume): CandleTick
    {
        $this->assetBuyVolume = $assetBuyVolume;

        return $this;
    }

    /**
     * @return float
     */
    public function getTakerBuyVolume(): float
    {
        return $this->takerBuyVolume;
    }

    /**
     * @param float $takerBuyVolume
     * @return CandleTick
     */
    public function setTakerBuyVolume(float $takerBuyVolume): CandleTick
    {
        $this->takerBuyVolume = $takerBuyVolume;

        return $this;
    }

    /**
     * @return int
     */
    public function getIgnored(): int
    {
        return $this->ignored;
    }

    /**
     * @param int $ignored
     * @return CandleTick
     */
    public function setIgnored(int $ignored): CandleTick
    {
        $this->ignored = $ignored;

        return $this;
    }
}
