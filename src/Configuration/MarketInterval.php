<?php declare(strict_types=1);

namespace App\Configuration;

final class MarketInterval
{
    // 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M

    public const MIN_1 = '1m';
    public const MIN_3 = '3m';
    public const MIN_5 = '5m';
    public const MIN_15 = '15m';
    public const HOUR_1 = '1h';
    public const HOUR_2 = '2h';
    public const HOUR_4 = '4h';
    public const HOUR_6 = '6h';
    public const HOUR_8 = '8h';
    public const HOUR_12 = '12h';
    public const DAY_1 = '1d';
    public const DAY_3 = '3d';
    public const WEEK_1 = '1w';
    public const MONTH_1 = '1M';
}
