<?php declare(strict_types=1);


namespace App\Service;

use _HumbugBoxebb6c51d1e3e\Symfony\Component\Console\Exception\LogicException;
use App\Configuration\OrderStatus;
use App\Entity\Laila;
use App\Entity\Market;
use App\Entity\Order;
use App\Event\OrderEvent;
use App\Facade\OrderFacade;
use App\Util\Util;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class OrderService
{
    /**
     * @var MarketService
     */
    private $marketService;

    /**
     * @var TradeService
     */
    private $tradeService;

    /**
     * @var OrderFacade
     */
    private $orderFacade;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * OrderService constructor.
     * @param MarketService            $marketService
     * @param OrderFacade              $orderFacade
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        MarketService $marketService,
        TradeService $tradeService,
        OrderFacade $orderFacade,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->marketService = $marketService;
        $this->tradeService = $tradeService;
        $this->orderFacade = $orderFacade;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Laila $laila
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function checkPositions(Laila $laila): void
    {
        if ($laila->getConfig()->isRunning() === false) {
            return;
        }
        foreach ($this->orderFacade->findOrdersToSync($laila->getId()) as $order) {
            $this->syncOrder($order);
            $this->checkTrades($order);
        }

        $market = $this->marketService->create($laila);

        foreach (
            $this->orderFacade->findOldWaitingOrders(
                $laila->getId(),
                Util::milisecondsToDt($market->getCandleTickData()->getLast()->getOpenTime())
            ) as $order) {
            $this->cancelOrder($order);
        }

        if ($laila->getConfig()->isOpenNewOrders() === true) {
            if ($this->orderFacade->countActiveOrders($laila->getId()) < $laila->getConfig()->getActiveOrderCount()) {
                $this->createOrder($market);
            }
        }
    }

    /**
     * @param Market $market
     * @return Order|null
     */
    public function createOrder(Market $market): ?Order
    {
        $order = $market->getOrder();
        if ($market->isOpen() === false) {
            return null;
        }
        $this->orderFacade->save($order);
        $this->eventDispatcher->dispatch(new OrderEvent($order), OrderEvent::CREATED);

        return $order;
    }

    /**
     * @param Order $order
     */
    public function sleepOrder(Order $order)
    {
        if ($order->getStatus() !== OrderStatus::OPEN) {
            throw new LogicException('bad_status_not_open');
        }
        $order->setStatus(OrderStatus::SLEEP);
        $this->orderFacade->save($order);
        $this->eventDispatcher->dispatch(new OrderEvent($order), OrderEvent::SLEPT);
    }

    /**
     * @param Order $order
     * @throws \ReflectionException
     */
    private function syncOrder(Order $order): void
    {
        foreach ($order->getTrades() as $trade) {
            $this->tradeService->syncTrade($trade);
        }
    }

    /**
     * @param Order $order
     * @throws \ReflectionException
     */
    private function checkTrades(Order $order): void
    {
        switch ($order->getStatus()) {
            case OrderStatus::CREATED:
            {
                if ($order->getActiveTrade(false) === null) {
                    $this->eventDispatcher->dispatch(new OrderEvent($order), OrderEvent::CREATED);
                }
                break;
            }
            case OrderStatus::OPEN:
            {
                if ($order->getActiveTrade(true) === null) {
                    $this->eventDispatcher->dispatch(new OrderEvent($order), OrderEvent::OPENED);
                }
                break;
            }
        }
    }

    /**
     * @param Order $order
     */
    private function cancelOrder(Order $order): void
    {
        foreach ($order->getTrades() as $trade) {
            $this->tradeService->cancelTrade($trade);
        }
    }
}
