<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation\Timestampable;
use JMS\Serializer\Annotation as JMS;

trait TimestampableTrait
{

    /**
     * @var DateTime|null $createdAt
     * @Timestampable(on="create")
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Exclude()
     */
    protected $createdAt;

    /**
     * @var DateTime|null $updatedAt
     * @Timestampable(on="update")
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Exclude()
     */
    protected $updatedAt;

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("createdAt")
     */
    public function getVirtualCreatedAtMilliseconds(): ?int
    {
        if ($this->createdAt === null) {
            return null;
        }

        return $this->createdAt->getTimestamp() * 1000;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return int
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("updatedAt")
     */
    public function getVirtualUpdatedAtMilliseconds(): ?int
    {
        if ($this->createdAt === null) {
            return null;
        }

        return $this->updatedAt->getTimestamp() * 1000;
    }
}
