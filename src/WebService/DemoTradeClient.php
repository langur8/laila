<?php declare(strict_types=1);

namespace App\WebService;

use App\Configuration\TradeSide;
use App\Configuration\TradeStatus;
use App\Entity\Trade;

final class DemoTradeClient implements TradeClientInterface
{
    /**
     * @var BinanceClient
     */
    private $binanceClient;

    /**
     * @param BinanceClient $binanceClient
     */
    public function __construct(BinanceClient $binanceClient)
    {
        $this->binanceClient = $binanceClient;
    }

    /**
     * @param Trade $trade
     * @throws \ReflectionException
     */
    public function getOrderStatus(Trade $trade): void
    {
        $data = $this->binanceClient->getCandleStickData(
            $trade->getSymbol(),
            $this->getDataInterval($trade->getTransactTime()),
            500
        );
        foreach ($data->getCandleTicks() as $candleTick) {
            if ($trade->getTransactTime() < $candleTick->getCloseTime() &&
                (
                    ($trade->getPrice() <= $candleTick->getHigh() && $trade->getSide() === TradeSide::SELL) ||
                    ($trade->getPrice() >= $candleTick->getLow() && $trade->getSide() === TradeSide::BUY)
                )) {
                $trade
                    ->setStatus(TradeStatus::FILLED)
                    ->setExecutedQty($trade->getOrigQty());

                return;
            }
        }
    }

    /**
     * @param Trade $trade
     * @throws \ReflectionException
     */
    public function cancel(Trade $trade): void
    {
        $trade
            ->setStatus(TradeStatus::CANCELED);
    }

    /**
     * @param int $time
     * @return string
     */
    private function getDataInterval(int $time): string
    {
        $timeDiff = time() - $time;
        if ($timeDiff < 500 * 60) {
            return '1m';
        }
        if ($timeDiff < 500 * 5 * 60) {
            return '5m';
        }
        if ($timeDiff < 500 * 60 * 60) {
            return '1h';
        }

        return '1d';

    }
}
