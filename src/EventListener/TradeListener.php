<?php declare(strict_types=1);

namespace App\EventListener;

use App\Configuration\OrderStatus;
use App\Event\OrderEvent;
use App\Event\TradeEvent;
use App\Facade\OrderFacade;
use App\Service\TradeService;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class TradeListener
{
    /**
     * @var OrderFacade
     */
    private $orderFacade;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var TradeService
     */
    private $tradeService;

    /**
     * TradeListener constructor.
     * @param OrderFacade              $orderFacade
     * @param EventDispatcherInterface $eventDispatcher
     * @param TradeService             $tradeService
     */
    public function __construct(
        OrderFacade $orderFacade,
        EventDispatcherInterface $eventDispatcher,
        TradeService $tradeService
    ) {
        $this->orderFacade = $orderFacade;
        $this->eventDispatcher = $eventDispatcher;
        $this->tradeService = $tradeService;
    }

    /**
     * @param TradeEvent $event
     */
    public function onTradePlaced(TradeEvent $event): void
    {
        $trade = $event->getTrade();
        $order = $trade->getOrder();
        $order
            ->setStatus($trade->isClosing() === true ? $order->getStatus() : OrderStatus::WAITING_OPEN)
            ->addTrade($trade);

        $this->orderFacade->save($order);
    }

    /**
     * @param TradeEvent $event
     */
    public function onTradeFilled(TradeEvent $event): void
    {
        $trade = $event->getTrade();
        $order = $trade->getOrder();
        $this->orderFacade->save(
            $order->setStatus($trade->isClosing() === true ? OrderStatus::CLOSED : OrderStatus::OPEN)
        );

        $this->eventDispatcher->dispatch(
            new OrderEvent($order), $trade->isClosing() === true ? OrderEvent::CLOSED : OrderEvent::OPENED
        );
    }

    /**
     * @param TradeEvent $event
     * @throws \ReflectionException
     */
    public function onTradeCanceled(TradeEvent $event): void
    {
        $trade = $event->getTrade();
        $order = $trade->getOrder();
        if ($trade->isClosing() === false) {
            $this->orderFacade->save(
                $order->setStatus(OrderStatus::CANCELED)
            );
        }
        $this->eventDispatcher->dispatch(
            new OrderEvent($order),
            $trade->isClosing() === true ? OrderEvent::CLOSE_CANCELED : OrderEvent::OPEN_CANCELED);

    }
}
