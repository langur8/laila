<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
final class UserController extends BaseRestController
{

    /**
     * @Rest\Get("/me")
     * @return Response
     */
    public function me(): Response
    {
        try {
            return $this->handleView($this->view($this->getUser()));
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}
