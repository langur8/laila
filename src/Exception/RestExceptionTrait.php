<?php declare(strict_types=1);

namespace App\Exception;

trait RestExceptionTrait
{
    /**
     * @var mixed[]|null
     */
    private $params;

    /**
     * @return string
     */
    public function getClientMessage(): string
    {
        return $this->clientMessage;
    }

    /**
     * @return array|null
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @param array|null $params
     * @return RestExceptionTrait
     */
    public function setParams(?array $params): self
    {
        $this->params = $params;

        return $this;
    }
}
