<?php declare(strict_types=1);

namespace App\Facade;

use App\Entity\FearAndGreedIndex;
use Doctrine\ORM\EntityManagerInterface;

final class FearAndGreedIndexFacade extends FacadeAbstract
{
    /**
     * @var string
     */
    protected $entityName = FearAndGreedIndex::class;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}
