<?php declare(strict_types=1);

namespace App\Exception;

final class JsonNotValidException extends \RuntimeException implements RestExceptionInterface
{
    use RestExceptionTrait;

    /**
     * @var string
     */
    private $clientMessage = 'response_exception';
}
