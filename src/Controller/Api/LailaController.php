<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use App\Service\LailaService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/laila")
 */
final class LailaController extends BaseRestController
{

    /**
     * @Rest\Post("")
     * @param Request      $request
     * @param LailaService $lailaService
     * @return Response
     */
    public function create(Request $request, LailaService $lailaService): Response
    {
        try {
            $data = json_decode($request->getContent(), true);
            if (empty($data['symbol'])) {
                throw new \InvalidArgumentException('no_symbol');
            }

            return $this->handleView($this->view($lailaService->create($data['symbol'])));
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * @Rest\Patch("")
     * @param Request      $request
     * @param LailaService $lailaService
     * @return Response
     */
    public function update(Request $request, LailaService $lailaService): Response
    {
        try {
            $data = json_decode($request->getContent(), true);

            return $this->handleView($this->view($lailaService->update($data)));
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * @Rest\Get("")
     * @param LailaService $lailaService
     * @return Response
     */
    public function getList(LailaService $lailaService): Response
    {
        try {
            return $this->handleView($this->view($lailaService->getList()));
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * @Rest\Get("/detail")
     * @param Request      $request
     * @param LailaService $lailaService
     * @return Response
     */
    public function detail(Request $request, LailaService $lailaService): Response
    {
        try {
            return $this->handleView($this->view(
                $lailaService->detail((int) $request->get('id', 0))
            ));
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * @Rest\Delete("")
     * @param LailaService $lailaService
     * @return Response
     */
    public function delete(Request $request, LailaService $lailaService): Response
    {
        try {
            $lailaService->delete((int) $request->get('id', 0));

            return new JsonResponse(null, Response::HTTP_ACCEPTED);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}
