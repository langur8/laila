<?php declare(strict_types=1);


namespace App\WebService;

use App\Util\Util;

final class ArrayToEntityMapper
{
    /**
     * @param array $array
     * @param       $entity
     * @param bool  $underscore
     * @throws \ReflectionException
     */
    public static function mapToEntity(array $array, $entity, bool $underscore = false): void
    {
        $reflection = new \ReflectionClass($entity);
        foreach ($reflection->getProperties() as $property) {
            $propertyName = self::getPropertyName($property, $underscore);
            if (isset($array[$propertyName])) {
                $setter = 'set' . ucfirst($property->getName());
                $value = static::retypeResultValue($property->getDocComment(), $array[$propertyName]);
                if ($value !== null) {
                    $entity->$setter($value);
                }
            }
        }
    }

    /**
     * @param string $docComment
     * @param        $value
     * @return mixed
     */
    private static function retypeResultValue(string $docComment, $value)
    {
        if ($value === null) {
            return null;
        }
        $search = ['int', 'float', 'bool', '\DateTime', 'DateTime', 'string', 'array'];
        foreach ($search as $type) {
            if (strstr($docComment, '@var ' . $type) !== false) {
                switch ($type) {
                    case 'string':
                        return (string) $value;
                    case 'int':
                        return (int) $value;
                    case 'float':
                        return (float) $value;
                    case 'bool':
                        return $value === '0' || $value === false ? false : true;
                    case 'array':
                        return is_array($value) ? $value : [];
                    case 'DateTime':
                    case '\DateTime':
                        try {
                            return new \DateTime($value);
                        } catch (\Exception $e) {
                            return $value;
                        }

                }
            }
        }

        return null;
    }

    /**
     * @param \ReflectionProperty $property
     * @param bool                $underscore
     * @return string
     */
    private static function getPropertyName(\ReflectionProperty $property, bool $underscore = false): string
    {
        return $underscore === true ? Util::toUnderscore($property->getName()) : $property->getName();
    }
}
