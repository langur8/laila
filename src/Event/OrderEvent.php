<?php declare(strict_types=1);

namespace App\Event;

use App\Entity\Order;
use Symfony\Contracts\EventDispatcher\Event;

final class OrderEvent extends Event
{
    public const CREATED = 'order.created';
    public const OPENED = 'order.opened';
    public const CLOSED = 'order.closed';
    public const OPEN_CANCELED = 'order.open_canceled';
    public const CLOSE_CANCELED = 'order.close_canceled';
    public const SLEPT = 'order.slept';

    /**
     * @var Order
     */
    private $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }
}
