<?php declare(strict_types=1);

namespace App\Entity;

use JMS\Serializer\Annotation as JMS;

final class CandleTickData
{
    /**
     * @var string
     */
    private $marketName;

    /**
     * @var string
     */
    private $interval;

    /**
     * @var CandleTick[]
     */
    private $candleTicks;

    /**
     * @return CandleTick|null
     */
    public function getCurrent(): ?CandleTick
    {
        if (count($this->candleTicks) === 0) {
            return null;
        }

        return $this->candleTicks[count($this->candleTicks) - 1];
    }

    /**
     * @return CandleTick|null
     */
    public function getLast(): ?CandleTick
    {
        if (count($this->candleTicks) === 0) {
            return null;
        }

        return $this->candleTicks[0];
    }

    /**
     * @param int $slice
     * @return float
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("spread")
     */
    public function getSpread(int $slice = 0): ?float
    {
        if (count($this->candleTicks) === 0) {
            return null;
        }

        return $this->getMax($slice) - $this->getMin($slice);
    }

    /**
     * @param int $slice
     * @return float|null
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("min")
     */
    public function getMin(int $slice = 0): ?float
    {
        $min = null;
        $candleTicks = array_slice($this->getCandleTicks(), $slice, count($this->getCandleTicks()));
        foreach ($candleTicks as $candleTick) {
            if ($min === null) {
                $min = $candleTick->getLow();
                continue;
            }
            $min = min($candleTick->getLow(), $min);
        }

        return $min;
    }

    /**
     * @param int $slice
     * @return float|null
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("max")
     */
    public function getMax(int $slice = 0): ?float
    {
        $max = null;
        $candleTicks = array_slice($this->getCandleTicks(), $slice, count($this->getCandleTicks()));
        foreach ($candleTicks as $candleTick) {
            if ($max === null) {
                $max = $candleTick->getHigh();
                continue;
            }
            $max = max($candleTick->getHigh(), $max);
        }

        return $max;
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("trend")
     * @param int $slice
     * @return float
     */
    public function getTrend(int $slice = 0): float
    {
        return $this->getCurrent()->getClose() - $this->getCloseAvg($slice);
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("trendArr")
     * @param int $slice
     * @return array
     */
    public function getTrendArr(int $slice = 0): array
    {
        $return = [];
        $avg = $this->getCloseAvg();
        $candleTicks = array_slice($this->getCandleTicks(), $slice, count($this->getCandleTicks()));
        foreach ($candleTicks as $key => $candleTick) {
            $return[] = $candleTick->getClose() - $avg;
        }

        return $return;
    }

    /**
     * @param int $slice
     * @return float
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("closeAvg")
     */
    public function getCloseAvg(int $slice = 0): float
    {
        $values = [];
        $candleTicks = array_slice($this->getCandleTicks(), $slice, count($this->getCandleTicks()));
        foreach ($candleTicks as $key => $candleTick) {
            $values[] = $candleTick->getClose();
        }

        return array_sum($values) / count($values);
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("closePercent")
     */
    public function getClosePercent(): float
    {
        $records = $this->getClosePercentArr();

        return count($records) === 0 ? 0.0 : array_sum($records) / count($records);
    }

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("closePercentArr")
     * @param int $slice
     * @return array
     */
    public function getClosePercentArr(int $slice = 0): array
    {
        $return = [];
        $candleTicks = array_slice($this->getCandleTicks(), $slice, count($this->getCandleTicks()));
        foreach ($candleTicks as $key => $candleTick) {
            $interval = ($candleTick->getHigh() - $candleTick->getLow()) / 2;
            $half = $candleTick->getHigh() - $interval;

            $return[] =
                $interval === 0.0 ?
                    0.0 :
                    ($candleTick->getClose() - $half) * 100 / $interval;
        }

        return $return;
    }

    /**
     * @param CandleTick $candleTick
     * @return CandleTickData
     */
    public function addCandleTick(CandleTick $candleTick): CandleTickData
    {
        $this->candleTicks[] = $candleTick;

        return $this;
    }

    /**
     * @return string
     */
    public function getMarketName(): string
    {
        return $this->marketName;
    }

    /**
     * @param string $marketName
     * @return CandleTickData
     */
    public function setMarketName(string $marketName): CandleTickData
    {
        $this->marketName = $marketName;

        return $this;
    }

    /**
     * @return string
     */
    public function getInterval(): string
    {
        return $this->interval;
    }

    /**
     * @param string $interval
     * @return CandleTickData
     */
    public function setInterval(string $interval): CandleTickData
    {
        $this->interval = $interval;

        return $this;
    }

    /**
     * @return CandleTick[]
     */
    public function getCandleTicks(): array
    {
        return $this->candleTicks;
    }
}
