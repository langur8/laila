<?php declare(strict_types=1);

namespace App\Facade;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

final class UserFacade extends FacadeAbstract
{
    /**
     * @var string
     */
    protected $entityName = User::class;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}
