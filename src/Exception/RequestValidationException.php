<?php declare(strict_types=1);

namespace App\Exception;

final class RequestValidationException extends \InvalidArgumentException implements RestExceptionInterface
{
    use RestExceptionTrait;

    /**
     * @var string
     */
    private $clientMessage = 'form_validation_error';

    /**
     * @var string[]
     */
    private $validation = [];

    /**
     * @return string[]
     */
    public function getValidation(): array
    {
        return $this->validation;
    }

    /**
     * @param string[] $validation
     * @return RequestValidationException
     */
    public function setValidation(array $validation): RequestValidationException
    {
        $this->validation = $validation;

        return $this;
    }
}
