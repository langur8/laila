<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class Laila
{
    use SoftDeletableTrait;
    use TimestampableTrait;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $symbol;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $baseAsset;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $quoteAsset;

    /**
     * @var Order[]|Collection
     * @ORM\OneToMany(targetEntity="Order", mappedBy="laila")
     * @JMS\Exclude()
     */
    private $orders;

    /**
     * @var Config
     * @ORM\OneToOne(targetEntity="Config", mappedBy="laila", cascade={"persist", "remove"})
     */
    private $config;

    /**
     * Laila constructor.
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Laila
     */
    public function setId(int $id): Laila
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return Laila
     */
    public function setSymbol(string $symbol): Laila
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return Order[]|Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param Order[]|Collection $orders
     * @return Laila
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @param Config $config
     * @return Laila
     */
    public function setConfig(Config $config): Laila
    {
        $this->config = $config;

        return $this;
    }

    /**
     * @return string
     */
    public function getBaseAsset(): string
    {
        return $this->baseAsset;
    }

    /**
     * @param string $baseAsset
     * @return Laila
     */
    public function setBaseAsset(string $baseAsset): Laila
    {
        $this->baseAsset = $baseAsset;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuoteAsset(): string
    {
        return $this->quoteAsset;
    }

    /**
     * @param string $quoteAsset
     * @return Laila
     */
    public function setQuoteAsset(string $quoteAsset): Laila
    {
        $this->quoteAsset = $quoteAsset;

        return $this;
    }
}
