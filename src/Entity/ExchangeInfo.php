<?php declare(strict_types=1);

namespace App\Entity;

use App\Util\Util;
use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\ExclusionPolicy("none")
 */
final class ExchangeInfo
{
    /**
     * @var string
     */
    private $symbol;

    /**
     * @var string|null
     */
    private $status;

    /**
     * @var string|null
     */
    private $baseAsset;

    /**
     * @var int|null
     */
    private $baseAssetPrecision;

    /**
     * @var string|null
     */
    private $quoteAsset;

    /**
     * @var int|null
     */
    private $quotePrecision;

    /**
     * @var int|null
     */
    private $baseCommissionPrecision;

    /**
     * @var int|null
     */
    private $quoteCommissionPrecision;

    /**
     * @var array|null
     */
    private $orderTypes;

    /**
     * @var bool|null
     */
    private $icebergAllowed;

    /**
     * @var bool|null
     */
    private $ocoAllowed;

    /**
     * @var bool|null
     */
    private $quoteOrderQtyMarketAllowed;

    /**
     * @var bool|null
     */
    private $isSpotTradingAllowed;

    /**
     * @var bool|null
     */
    private $isMarginTradingAllowed;

    /**
     * @var array|null
     */
    private $filters;

    /**
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("minNotation")
     * @return float|null
     */
    public function getMinNotation(): ?float
    {
        foreach ($this->filters as $filter) {
            if ($filter['filterType'] === 'MIN_NOTIONAL') {
                return (float) $filter['minNotional'];
            }
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getLotDecimals(): ?int
    {
        foreach ($this->filters as $filter) {
            if ($filter['filterType'] === 'LOT_SIZE') {
                return Util::countDecimals((float) $filter['stepSize']);
            }
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getMinPriceDecimals(): ?int
    {
        foreach ($this->filters as $filter) {
            if ($filter['filterType'] === 'PRICE_FILTER') {
                return Util::countDecimals((float) $filter['minPrice']);
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return ExchangeInfo
     */
    public function setSymbol(string $symbol): ExchangeInfo
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return ExchangeInfo
     */
    public function setStatus(?string $status): ExchangeInfo
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBaseAsset(): ?string
    {
        return $this->baseAsset;
    }

    /**
     * @param string|null $baseAsset
     * @return ExchangeInfo
     */
    public function setBaseAsset(?string $baseAsset): ExchangeInfo
    {
        $this->baseAsset = $baseAsset;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getBaseAssetPrecision(): ?int
    {
        return $this->baseAssetPrecision;
    }

    /**
     * @param int|null $baseAssetPrecision
     * @return ExchangeInfo
     */
    public function setBaseAssetPrecision(?int $baseAssetPrecision): ExchangeInfo
    {
        $this->baseAssetPrecision = $baseAssetPrecision;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getQuoteAsset(): ?string
    {
        return $this->quoteAsset;
    }

    /**
     * @param string|null $quoteAsset
     * @return ExchangeInfo
     */
    public function setQuoteAsset(?string $quoteAsset): ExchangeInfo
    {
        $this->quoteAsset = $quoteAsset;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuotePrecision(): ?int
    {
        return $this->quotePrecision;
    }

    /**
     * @param int|null $quotePrecision
     * @return ExchangeInfo
     */
    public function setQuotePrecision(?int $quotePrecision): ExchangeInfo
    {
        $this->quotePrecision = $quotePrecision;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getBaseCommissionPrecision(): ?int
    {
        return $this->baseCommissionPrecision;
    }

    /**
     * @param int|null $baseCommissionPrecision
     * @return ExchangeInfo
     */
    public function setBaseCommissionPrecision(?int $baseCommissionPrecision): ExchangeInfo
    {
        $this->baseCommissionPrecision = $baseCommissionPrecision;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuoteCommissionPrecision(): ?int
    {
        return $this->quoteCommissionPrecision;
    }

    /**
     * @param int|null $quoteCommissionPrecision
     * @return ExchangeInfo
     */
    public function setQuoteCommissionPrecision(?int $quoteCommissionPrecision): ExchangeInfo
    {
        $this->quoteCommissionPrecision = $quoteCommissionPrecision;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOrderTypes(): ?array
    {
        return $this->orderTypes;
    }

    /**
     * @param array|null $orderTypes
     * @return ExchangeInfo
     */
    public function setOrderTypes(?array $orderTypes): ExchangeInfo
    {
        $this->orderTypes = $orderTypes;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIcebergAllowed(): ?bool
    {
        return $this->icebergAllowed;
    }

    /**
     * @param bool|null $icebergAllowed
     * @return ExchangeInfo
     */
    public function setIcebergAllowed(?bool $icebergAllowed): ExchangeInfo
    {
        $this->icebergAllowed = $icebergAllowed;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getOcoAllowed(): ?bool
    {
        return $this->ocoAllowed;
    }

    /**
     * @param bool|null $ocoAllowed
     * @return ExchangeInfo
     */
    public function setOcoAllowed(?bool $ocoAllowed): ExchangeInfo
    {
        $this->ocoAllowed = $ocoAllowed;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getQuoteOrderQtyMarketAllowed(): ?bool
    {
        return $this->quoteOrderQtyMarketAllowed;
    }

    /**
     * @param bool|null $quoteOrderQtyMarketAllowed
     * @return ExchangeInfo
     */
    public function setQuoteOrderQtyMarketAllowed(?bool $quoteOrderQtyMarketAllowed): ExchangeInfo
    {
        $this->quoteOrderQtyMarketAllowed = $quoteOrderQtyMarketAllowed;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsSpotTradingAllowed(): ?bool
    {
        return $this->isSpotTradingAllowed;
    }

    /**
     * @param bool|null $isSpotTradingAllowed
     * @return ExchangeInfo
     */
    public function setIsSpotTradingAllowed(?bool $isSpotTradingAllowed): ExchangeInfo
    {
        $this->isSpotTradingAllowed = $isSpotTradingAllowed;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsMarginTradingAllowed(): ?bool
    {
        return $this->isMarginTradingAllowed;
    }

    /**
     * @param bool|null $isMarginTradingAllowed
     * @return ExchangeInfo
     */
    public function setIsMarginTradingAllowed(?bool $isMarginTradingAllowed): ExchangeInfo
    {
        $this->isMarginTradingAllowed = $isMarginTradingAllowed;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getFilters(): ?array
    {
        return $this->filters;
    }

    /**
     * @param array|null $filters
     * @return ExchangeInfo
     */
    public function setFilters(?array $filters): ExchangeInfo
    {
        $this->filters = $filters;

        return $this;
    }
}
