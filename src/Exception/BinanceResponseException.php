<?php declare(strict_types=1);

namespace App\Exception;

final class BinanceResponseException extends \RuntimeException implements RestExceptionInterface
{
    use RestExceptionTrait;

    /**
     * @var string
     */
    private $clientMessage = 'binance_exception';
}
