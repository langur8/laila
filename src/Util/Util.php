<?php declare(strict_types=1);

namespace App\Util;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;

final class Util
{
    /**
     * Make object or value iterable
     * @param mixed $object
     * @return array
     */
    public static function iterable($object)
    {
        if (empty($object)) {
            return [];
        } elseif (!is_array($object)) {
            return [$object];
        }

        return $object;
    }

    /**
     * Return first array element
     * @param mixed $array
     * @return mixed|null
     */
    public static function getFirst($array)
    {
        foreach (self::iterable($array) as $item) {
            return $item;
        }

        return null;
    }

    /**
     * Recursive find in array
     * @param array  $data
     * @param string $needle
     * @return null
     */
    public static function findArrayKeyValue($data, $needle)
    {
        foreach ($data as $key => $value) {
            if ($key === $needle) {
                return $value;
            }

            if (is_array($value)) {
                $search = self::findArrayKeyValue($value, $needle);
                if (!empty($search)) {
                    return $search;
                }
            }
        }

        return null;
    }

    /**
     * Get errors messages from Symfony form component
     * @param FormInterface $form
     * @return array
     */
    public static function getErrorsFromForm(FormInterface $form): array
    {
        $errors = [];
        /** @var FormError $error */
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                $childErrors = static::getErrorsFromForm($childForm);
                if ($childErrors) {
                    $err['error'] = implode(',', $childErrors);
                    $errors[] = $err;
                }
            }
        }

        return $errors;
    }

    /**
     * @param ConstraintViolationListInterface $list
     * @return array
     */
    public static function getErrorsFromConstraints(ConstraintViolationListInterface $list): array
    {
        $return = [];
        if ($list->count() === 0) {
            return $return;
        }
        /** @var ConstraintViolation $constraintViolation */
        foreach ($list as $constraintViolation) {
            $return [str_replace(['[', ']'], '', $constraintViolation->getPropertyPath())] =
                $constraintViolation->getMessage();
        }

        return $return;
    }

    /**
     * @param ConstraintViolationListInterface $list
     * @return array
     */
    public static function getParamsFromConstraints(ConstraintViolationListInterface $list): array
    {
        $return = [];
        if ($list->count() === 0) {
            return $return;
        }
        /** @var ConstraintViolation $constraintViolation */
        foreach ($list as $constraintViolation) {
            $return [str_replace(['[', ']'], '', $constraintViolation->getPropertyPath())] =
                $constraintViolation->getInvalidValue();
        }

        return $return;
    }

    /**
     * @param array $result
     * @return array
     */
    public static function getIndexedArray(array $result): array
    {
        $return = [];
        foreach ($result as $value) {
            $return[$value->getId()] = $value;
        }

        return $return;
    }

    /**
     * @param int|string $miliseconds
     * @return \DateTime
     * @throws \Exception
     */
    public static function milisecondsToDt($miliseconds): \DateTime
    {
        return (new \DateTime())->setTimestamp((int) round((int) $miliseconds / 1000));
    }

    /**
     * @param \DateTime $datetime
     * @return int
     */
    public static function dtToMiliseconds(\DateTime $datetime): int
    {
        return $datetime->getTimestamp() * 1000;
    }

    /**
     * @param \DateTime      $dateOfBirth
     * @param \DateTime|null $base
     * @return int
     * @throws \Exception
     */
    public static function getAge(\DateTime $dateOfBirth, ?\DateTime $base = null): int
    {
        $base = $base ?? new \DateTime();

        if ((int) $dateOfBirth->format('Ymd') > (int) $base->format('Ymd')) {
            throw new \InvalidArgumentException('date_of_birth_greater_than_base');
        }

        return $dateOfBirth->diff($base)->y;
    }

    /**
     * @param int    $price
     * @return int
     */
    public static function roundPrice(int $price): int
    {
        return (int) (ceil($price / 100)) * 100;
    }

    /**
     * @param int       $typeNo
     * @param int       $packNo
     * @param \DateTime $validFrom
     * @param int       $famSortNo
     * @return string
     */
    public static function generateProductHashInput(
        int $typeNo,
        int $packNo,
        \DateTime $validFrom,
        int $famSortNo
    ): string {
        return $typeNo . $packNo . $validFrom->format('Ymd') . $famSortNo;
    }

    /**
     * @param string $value
     * @param int    $length
     * @return int
     */
    public static function crc1(string $value, int $length = 1): int
    {
        $crc = (string) crc32($value);

        return (int) substr($crc, strlen($crc) - $length, $length);
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param string    $locale
     * @return string
     */
    public static function validity(\DateTime $dateFrom, \DateTime $dateTo, string $locale): string
    {
        $format = 'd.m.y';
        if ($locale === 'en') {
            $format = 'm/d/y';
        }
        if ($dateFrom->format($format) === $dateTo->format($format)) {
            return $dateFrom->format($format);
        }

        return $dateFrom->format($format) . ' - ' . $dateTo->format($format);
    }

    /**
     * @param \DateTime $dateFrom
     * @param string    $locale
     * @return string
     */
    public static function date(\DateTime $dateFrom, string $locale): string
    {
        $format = 'd.m.y';
        if ($locale === 'en') {
            $format = 'm/d/y';
        }

        return $dateFrom->format($format);
    }

    /**
     * @param int    $price
     * @param string $currency
     * @return string
     */
    public static function price(int $price, string $currency): string
    {
        return number_format(
                $price / 100,
                2,
                ',',
                ' '
            ) . ' ' . $currency;
    }

    /**
     * @param string $hexString
     * @return array
     */
    public static function hex2Rgb(string $hexString): array
    {
        $hexString = preg_replace('/[^0-9A-Fa-f]/', '', $hexString);
        $rgbArray = [];
        if (strlen($hexString) === 6) {
            $colorVal = hexdec($hexString);
            $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue'] = 0xFF & $colorVal;
        } elseif (strlen($hexString) === 3) {
            $rgbArray['red'] = hexdec(str_repeat(substr($hexString, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexString, 1, 1), 2));
            $rgbArray['blue'] = hexdec(str_repeat(substr($hexString, 2, 1), 2));
        }

        return $rgbArray;
    }

    /**
     * @param string|null $email
     * @return string
     */
    public static function maskEmail(?string $email): string
    {
        if ($email === null) {
            return '';
        }
        $arrEmail = explode('@', $email);
        if (count($arrEmail) !== 2) {
            return '';
        }
        $domain = explode('.', $arrEmail[1]);
        if (count($domain) !== 2) {
            return '';
        }

        return substr($arrEmail[0], 0, 1) .
            '***@' .
            substr($domain[0], 0, 1) .
            '***.'
            . $domain[1];
    }

    /**
     * @param string $string
     * @return string
     */
    public static function toUnderscore(string $input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    /**
     * @param float $fNumber
     * @return int
     */
    public static function countDecimals(float $fNumber): int
    {
        for ($iDecimals = 0; $fNumber != round($fNumber, $iDecimals); $iDecimals++) {
            continue;
        }

        return $iDecimals;
    }

    /**
     * @param float $fNumber
     * @return int
     */
    public static function zeroDecimalPlaces(float $fNumber): int
    {
        for ($base = 1, $count = 0; abs($fNumber * $base) < 1; $base *= 10, $count++) {
            continue;
        }

        return $count;
    }

    /**
     * @param float $fNumber
     * @param int   $decimals
     * @return float
     */
    public static function intelRound(float $fNumber, int $decimals = 2): float
    {
        return (float) round($fNumber, (abs($fNumber) > 1 ? 0 : self::zeroDecimalPlaces($fNumber)) + $decimals);
    }
}
