<?php declare(strict_types=1);


namespace App\Service;

use App\Entity\Laila;
use App\Entity\Config;
use App\Facade\LailaFacade;
use App\WebService\ArrayToEntityMapper;
use App\WebService\BinanceClient;

class LailaService
{
    /**
     * @var LailaFacade
     */
    private $lailaFacade;

    /**
     * @var BinanceClient
     */
    private $binanceClient;

    /**
     * @param LailaFacade $lailaFacade
     */
    public function __construct(LailaFacade $lailaFacade, BinanceClient $binanceClient)
    {
        $this->lailaFacade = $lailaFacade;
        $this->binanceClient = $binanceClient;
    }

    /**
     * @param string $symbol
     * @return Laila
     * @throws \Exception
     */
    public function create(string $symbol): Laila
    {
        $laila = new Laila;
        $config = new Config();
        $exchangeInfo = $this->binanceClient->exchangeInfo($symbol);
        $this->lailaFacade->save(
            $laila
                ->setSymbol($symbol)
                ->setBaseAsset($exchangeInfo->getBaseAsset())
                ->setQuoteAsset($exchangeInfo->getQuoteAsset())
                ->setConfig(
                    $config
                        ->setLaila($laila)
                        ->setOrderAmount(
                            $exchangeInfo->getMinNotation()
                        )
                        ->setOrderAmountPrecision($exchangeInfo->getLotDecimals())
                        ->setPricePrecision($exchangeInfo->getMinPriceDecimals())
                )
        );

        return $laila;
    }

    /**
     * @param array $data
     * @return Laila
     * @throws \ReflectionException
     */
    public function update(array $data): Laila
    {
        if (!isset($data['id'])) {
            throw new \InvalidArgumentException('no_id');
        }
        $laila = $this->lailaFacade->findOneBy(['id' => (int) $data['id'], 'deletedAt' => null]);
        if (!$laila instanceof Laila) {
            throw new \InvalidArgumentException('not_found');
        }
        ArrayToEntityMapper::mapToEntity($data, $laila);
        ArrayToEntityMapper::mapToEntity($data['config'], $laila->getConfig());
        $this->lailaFacade->save($laila);

        return $laila;
    }

    /**
     * @return Laila[]
     */
    public function getList(): array
    {
        return $this->lailaFacade->findBy(['deletedAt' => null]);
    }

    /**
     * @param int $id
     * @return Laila|object
     */
    public function detail(int $id)
    {
        return $this->lailaFacade->findOneBy(['deletedAt' => null, 'id' => $id]);
    }

    /**
     * @param int $id
     * @throws \Exception
     */
    public function delete(int $id): void
    {
        $entity = $this->lailaFacade->find($id);
        if (!$entity instanceof Laila) {
            throw new \InvalidArgumentException('not_found');
        }
        $this->lailaFacade->delete($entity);
    }
}
