<?php declare(strict_types=1);

namespace App\Configuration;

final class TradeStatus
{
    public const NEW = 'NEW';
    public const PARTIALLY_FILLED = 'PARTIALLY_FILLED';
    public const FILLED = 'FILLED';
    public const CANCELED = 'CANCELED';
    public const PENDING_CANCEL = 'PENDING_CANCEL';
    public const REJECTED = 'REJECTED';
    public const EXPIRED = 'EXPIRED';

    /**
     * @param string $state
     * @return bool
     */
    public static function isOpen(string $state): bool
    {
        return in_array($state, [self::NEW, self::PARTIALLY_FILLED, self::PENDING_CANCEL]);
    }
}
