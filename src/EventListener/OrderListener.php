<?php declare(strict_types=1);

namespace App\EventListener;

use App\Configuration\OrderStatus;
use App\Event\OrderEvent;
use App\Service\TradeService;

final class OrderListener
{

    /**
     * @var TradeService
     */
    private $tradeService;

    public function __construct(TradeService $tradeService)
    {
        $this->tradeService = $tradeService;
    }

    /**
     * @param OrderEvent $event
     * @throws \ReflectionException
     */
    public function onOrderCreated(OrderEvent $event): void
    {
        $order = $event->getOrder();
        if ($order->getStatus() !== OrderStatus::CREATED) {
            throw new \LogicException('bad_status');
        }
        $this->tradeService->createOpeningTrade($order);
    }

    /**
     * @param OrderEvent $event
     * @throws \ReflectionException
     */
    public function onOrderOpened(OrderEvent $event): void
    {
        $order = $event->getOrder();
        if ($order->getStatus() !== OrderStatus::OPEN) {
            throw new \LogicException('bad_status');
        }
        $this->tradeService->createClosingTrade($order);
    }

    public function onOrderClosed(OrderEvent $event): void
    {
    }

    public function onOrderOpenCanceled(OrderEvent $event): void
    {
    }

    /**
     * @param OrderEvent $event
     * @throws \ReflectionException
     */
    public function onOrderCloseCanceled(OrderEvent $event): void
    {
        $order = $event->getOrder();
        if ($order->getStatus() !== OrderStatus::WAITING_CLOSE) {
            throw new \LogicException('bad_status');
        }
        $this->tradeService->createClosingTrade($order);
    }

    public function onOrderSlept(OrderEvent $event): void
    {
    }
}
