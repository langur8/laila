<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use App\WebService\BinanceClient;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/exchange")
 */
final class ExchangeController extends BaseRestController
{
    /**
     * @Rest\Get("/info")
     * @param Request       $request
     * @param BinanceClient $binanceClient
     * @return Response
     */
    public function create(Request $request, BinanceClient $binanceClient): Response
    {
        try {
            return $this->handleView($this->view(
                $binanceClient->exchangeInfo($request->get('symbol', ''))
            ));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

}
