<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table()
 */
class FearAndGreedIndex
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $timestamp;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $valueClassification;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $timeUntilUpdate;

    /**
     * @return bool
     */
    public function isCurrent(): bool
    {
        return $this->getTimestamp() + $this->getTimeUntilUpdate() > time();
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return FearAndGreedIndex
     */
    public function setValue(int $value): FearAndGreedIndex
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getValueClassification(): string
    {
        return $this->valueClassification;
    }

    /**
     * @param string $valueClassification
     * @return FearAndGreedIndex
     */
    public function setValueClassification(string $valueClassification): FearAndGreedIndex
    {
        $this->valueClassification = $valueClassification;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    /**
     * @param int $timestamp
     * @return FearAndGreedIndex
     */
    public function setTimestamp(int $timestamp): FearAndGreedIndex
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimeUntilUpdate(): int
    {
        return $this->timeUntilUpdate;
    }

    /**
     * @param int $timeUntilUpdate
     * @return FearAndGreedIndex
     */
    public function setTimeUntilUpdate(int $timeUntilUpdate): FearAndGreedIndex
    {
        $this->timeUntilUpdate = $timeUntilUpdate;

        return $this;
    }
}
