<?php declare(strict_types=1);


namespace App\Controller;

use App\Entity\RestErrorMessage;
use App\Exception\RequestValidationException;
use App\Exception\RestExceptionInterface;
use App\Util\Util;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Psr\Log\LoggerInterface;
use Sentry\ClientInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseRestController extends AbstractFOSRestController
{
    /** @var LoggerInterface */
    private $restLogger;

    /** @var ClientInterface */
    private $sentry;

    /**
     * @param LoggerInterface $restLogger
     */
    public function __construct(LoggerInterface $restLogger, ClientInterface $sentry)
    {
        $this->sentry = $sentry;
        $this->restLogger = $restLogger;
    }

    /**
     * @param Exception $exception
     * @param int       $responseCode
     * @return Response
     */
    protected function exceptionResponse(Exception $exception, int $responseCode = Response::HTTP_BAD_REQUEST): Response
    {
        $message = 'internal_error';
        $error = $exception->getMessage();
        if ($exception instanceof RestExceptionInterface) {
            $message = $exception->getClientMessage();
            $error = $exception->getMessage();
        }
        $params = null;
        if (method_exists($exception, 'getParams')) {
            $params = $exception->getParams();
        }
        $validation = null;
        if (method_exists($exception, 'getValidation')) {
            $validation = $exception->getValidation();
            $responseCode = Response::HTTP_BAD_REQUEST;
        } elseif (method_exists($exception, 'getStatusCode') && $exception->getStatusCode() > 0) {
            $responseCode = $exception->getStatusCode();
        } elseif (!$exception instanceof RestExceptionInterface) {
            $responseCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        if ($validation === null) {
            $this->sentry->captureException($exception);
        }

        return $this->returnError(
            new RestErrorMessage($message, $error, $exception->getCode(), $validation, $params),
            $responseCode
        );
    }

    /**
     * @param RestErrorMessage $errorMessage
     * @param int              $code
     * @return Response
     */
    protected function returnError(RestErrorMessage $errorMessage, int $code = Response::HTTP_BAD_REQUEST): Response
    {
        if ($errorMessage->getValidation() !== null) {
            return $this->handleView(
                $this->view($errorMessage->toArray(), $code)
            );
        }

        $this->restLogger->error($errorMessage->getMessage());

        return $this->handleView(
            $this->view($errorMessage->toArray(), $code)
        );
    }

    /**
     * @param ConstraintViolationListInterface $violationList
     * @return Response
     */
    protected function validationError(ConstraintViolationListInterface $violationList): Response
    {
        return $this->returnError(
            new RestErrorMessage(
                'form_validation_error',
                'form_validation_error',
                0,
                Util::getErrorsFromConstraints($violationList))
        );
    }

    /**
     * @param ValidatorInterface $validator
     * @param                    $request
     * @param array|null         $groups
     */
    protected function validate(ValidatorInterface $validator, $request, ?array $groups = null): void
    {
        $violations = $validator->validate($request, null, $groups);
        if ($violations->count() > 0) {
            $exception = new RequestValidationException();
            $exception->setValidation(Util::getErrorsFromConstraints($violations));
            $exception->setParams(Util::getParamsFromConstraints($violations));
            throw $exception;
        }
    }
}
