<?php declare(strict_types=1);


namespace App\Service;

use App\Configuration\OrderStatus;
use App\Configuration\TradeSide;
use App\Configuration\Trend;
use App\Entity\Config;
use App\Entity\Laila;
use App\Entity\Market;
use App\Entity\Order;
use App\WebService\BinanceClient;

final class MarketService
{
    /**
     * @var BinanceClient
     */
    private $binanceClient;

    /**
     * @var FearAndGreedIndexService
     */
    private $fearAndGreedIndexService;

    /**
     * OrderService constructor.
     * @param BinanceClient $binanceClient
     */
    public function __construct(
        BinanceClient $binanceClient,
        FearAndGreedIndexService $fearAndGreedIndexService
    ) {
        $this->binanceClient = $binanceClient;
        $this->fearAndGreedIndexService = $fearAndGreedIndexService;
    }

    /**
     * @param Laila $laila
     * @return Order
     * @throws \ReflectionException
     */
    public function create(Laila $laila): Market
    {
        $market = new Market();
        $market
            ->setLaila($laila)
            ->setSymbol($laila->getSymbol())
            ->setCandleTickData(
                $this->binanceClient->getCandleStickData(
                    $laila->getSymbol(),
                    $laila->getConfig()->getAnalyzeInterval(),
                    $laila->getConfig()->getAnalyzeTicks()
                )
            );
        $this->setFnG($market);
        $this->setPrevDay($market);
        $this->setPrices($market);
        $this->setOrder($market);
        $this->setOpen($market);

        return $market;
    }

    /**
     * @param Market $market
     */
    private function setOpen(Market $market): void
    {
        if ($market->getOrder()->getSide() === TradeSide::BUY) {
            if ($market->getOrder()->getTargetPrice() > $market->getMaxBuyPrice()) {
                $market->setOpenMsg('target_price');
                $market->setOpen(false);

                return;
            }
            if ($market->getCandleTickData()->getTrend() < 0) {
                $market->setOpenMsg('trend');
                $market->setOpen(false);

                return;
            }
            if ($market->getCandleTickData()->getClosePercent() < 0) {
                $market->setOpenMsg('close_percent');
                $market->setOpen(false);

                return;
            }
            if ($market->getCandleTickData()->getTrend($market->getLaila()->getConfig()->getAnalyzeTicks() - 3) > 0) {
                $market->setOpenMsg('current_move');
                $market->setOpen(false);

                return;
            }
            $market->setOpen(true);
            $market->setOpenMsg('ok');

            return;
        }

        if ($market->getOrder()->getTargetPrice() < $market->getMinSellPrice()) {
            $market->setOpenMsg('target_price');
            $market->setOpen(false);

            return;
        }
        if ($market->getCandleTickData()->getTrend() > 0) {
            $market->setOpenMsg('trend');
            $market->setOpen(false);

            return;
        }
        if ($market->getCandleTickData()->getClosePercent() > 0) {
            $market->setOpenMsg('close_percent');
            $market->setOpen(false);

            return;
        }
        if ($market->getCandleTickData()->getTrend($market->getLaila()->getConfig()->getAnalyzeTicks() - 3) < 0) {
            $market->setOpenMsg('current_move');
            $market->setOpen(false);

            return;
        }
        $market->setOpen(true);
        $market->setOpenMsg('ok');

        return;
    }

    /**
     * @param Market $market
     * @throws \ReflectionException
     */
    private function setFnG(Market $market): void
    {
        $data = $this->fearAndGreedIndexService->getIndex(2);
        if ($data->getCurrent() === null) {
            return;
        }
        $market
            ->setFearAndGreed($data->getCurrent()->getValue())
            ->setFearAndGreedName($data->getCurrent()->getValueClassification())
            ->setFearAndGreedChange($data->getLastShift());
    }

    /**
     * @param Market $market
     * @throws \ReflectionException
     */
    private function setPrevDay(Market $market): void
    {
        $prevDay = $this->binanceClient->getPrevDay($market->getSymbol());
        $market
            ->setPrevDay($prevDay);
    }

    /**
     * @param Market $market
     */
    private function setPrices(Market $market): void
    {
        $prevDay = $market->getPrevDay();
        $config = $market->getLaila()->getConfig();
        $max = $prevDay->getHighPrice();
        $min = $prevDay->getLowPrice();
        $interval = $max - $min;
        $market
            ->setPrice($market->getCandleTickData()->getCurrent()->getClose())
            ->setMaxBuyPrice($max - ($config->getProtectedZoneUp() * $interval))
            ->setMinSellPrice($min + ($config->getProtectedZoneDown() * $interval));
    }

    /**
     * @param Market $market
     */
    private function setOrder(Market $market): void
    {
        $config = $market->getLaila()->getConfig();
        $order = new Order;
        $order
            ->setSymbol($market->getSymbol())
            ->setLaila($market->getLaila())
            ->setStatus(OrderStatus::CREATED)
            ->setDemoMode($config->isDemoMode())
            ->setSide($config->getTrend() === Trend::DOWN ? TradeSide::SELL : TradeSide::BUY);

        $this->setTarget($this->getOpenPrice($market), $order, $config);

        $market->setOrder($order);
    }

    /**
     * @param Market $market
     * @return float
     */
    private function getOpenPrice(Market $market): float
    {
        $config = $market->getLaila()->getConfig();
        $avg = round($market->getCandleTickData()->getCloseAvg(), $config->getPricePrecision());
        $currentPrice = $market->getCandleTickData()->getCurrent()->getClose();

        return $config->getTrend() === Trend::DOWN ? max($avg, $currentPrice) : min($avg, $currentPrice);
    }

    /**
     * @param float  $price
     * @param Order  $order
     * @param Config $config
     */
    private function setTarget(float $price, Order $order, Config $config): void
    {
        $openAmount = $this->getOpenAmount($price, $config);
        $openFee = $this->getFeePercent($openAmount, $config);
        if ($config->isClaimLeftSide()) {
            $closeAmount = round($openAmount - $this->getTargetPercent($openAmount, $config), $config->getOrderAmountPrecision());
            $closeFee = $this->getFeePercent($closeAmount, $config);
            $targetPrice = $price + $this->getTargetPercent($price, $config);
            $profit = ($openAmount - $closeAmount) * $config->getTrendCoef() - $openFee - $closeFee;
            $profitCurrency = $order->getLaila()->getBaseAsset();
        } else {
            $closeAmount = $openAmount;
            $closeFee = $this->getFeePercent($closeAmount, $config);
            $targetPrice = $price + $this->getTargetPercent($price, $config);
            $profit = (($targetPrice * $closeAmount) - ($price * $openAmount)) * $config->getTrendCoef() - ($openFee * $price) - ($closeFee * $price);
            $profitCurrency = $order->getLaila()->getQuoteAsset();
        }

        $order
            ->setOpenAmount($openAmount)
            ->setTargetAmount($closeAmount)
            ->setFees($openFee + $closeFee)
            ->setOpenPrice($price)
            ->setTargetPrice(round($targetPrice, $config->getPricePrecision()))
            ->setProfit($profit)
            ->setProfitCurrency($profitCurrency);

    }

    /**
     * @param float  $price
     * @param Config $config
     * @return float
     */
    private function getOpenAmount(float $price, Config $config): float
    {
        return round($config->getOrderAmount() / $price, $config->getOrderAmountPrecision());
    }

    /**
     * @param float  $value
     * @param Config $config
     * @return float
     */
    private function getTargetPercent(float $value, Config $config): float
    {
        return ($config->getTakeProfitPercentage()) * ($value / 100) * $config->getTrendCoef();
    }

    /**
     * @param float  $value
     * @param Config $config
     * @return float
     */
    private function getFeePercent(float $value, Config $config): float
    {
        return ($config->getFees()) * ($value / 100);
    }
}
