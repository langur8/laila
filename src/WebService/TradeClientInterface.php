<?php declare(strict_types=1);

namespace App\WebService;

use App\Entity\Trade;

interface TradeClientInterface
{
    /**
     * @param Trade $trade
     */
    public function getOrderStatus(Trade $trade): void;

    /**
     * @param Trade $trade
     */
    public function cancel(Trade $trade): void;
}
