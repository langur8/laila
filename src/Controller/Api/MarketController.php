<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use App\Entity\Laila;
use App\Facade\LailaFacade;
use App\Service\MarketService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/market")
 */
final class MarketController extends BaseRestController
{
    /**
     * @Rest\Get("")
     * @param Request       $request
     * @param MarketService $marketService
     * @param LailaFacade   $lailaFacade
     * @return Response
     */
    public function create(Request $request, MarketService $marketService, LailaFacade $lailaFacade): Response
    {
        try {
            $id = $request->get('id', 0);
            if (!$id) {
                throw new \InvalidArgumentException('id');
            }
            $laila = $lailaFacade->find((int) $id);
            if (!$laila instanceof Laila) {
                throw new \InvalidArgumentException('not_found');
            }

            return $this->handleView($this->view(
                $marketService->create($laila)
            ));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

}
