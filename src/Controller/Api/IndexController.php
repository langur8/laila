<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use App\Service\FearAndGreedIndexService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BalanceController
 * @package App\Controller\Api
 * @Route("/index")
 */
final class IndexController extends BaseRestController
{
    /**
     * @Rest\Get("/fng")
     * @param Request                  $request
     * @param FearAndGreedIndexService $service
     * @return Response
     */
    public function fng(Request $request, FearAndGreedIndexService $service): Response
    {
        try {
            $limit = (int) $request->get('limit', 1);

            return $this->handleView($this->view($service->getIndex($limit)));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }
}
