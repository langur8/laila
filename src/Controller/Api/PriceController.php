<?php declare(strict_types=1);

namespace App\Controller\Api;

use App\Controller\BaseRestController;
use App\WebService\BinanceClient;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/price")
 */
final class PriceController extends BaseRestController
{
    /**
     * @Rest\Get("/current")
     * @param Request       $request
     * @param BinanceClient $binanceClient
     * @return Response
     */
    public function price(Request $request, BinanceClient $binanceClient): Response
    {
        try {
            $symbol = $request->get('symbol');

            return $this->handleView($this->view([$symbol => $binanceClient->getPrice($symbol)]));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Get("/all")
     * @param BinanceClient $binanceClient
     * @return Response
     */
    public function prices(BinanceClient $binanceClient): Response
    {
        try {
            return $this->handleView($this->view($binanceClient->getPrices()));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Get("/candle-stick")
     * @param Request       $request
     * @param BinanceClient $binanceClient
     * @return Response
     */
    public function candleStick(Request $request, BinanceClient $binanceClient): Response
    {
        try {
            $symbol = $request->get('symbol');
            $interval = $request->get('interval');
            $limit = (int) $request->get('limit', 60);

            return $this->handleView($this->view($binanceClient->getCandleStickData($symbol, $interval, $limit)));
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Get("/prev-day")
     * @param Request       $request
     * @param BinanceClient $binanceClient
     * @return Response
     */
    public function prevDay(Request $request, BinanceClient $binanceClient): Response
    {
        try {
            return $this->handleView(
                $this->view(
                    $binanceClient->getPrevDay(
                        $request->get('symbol')
                    )
                )
            );
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Get("/depth")
     * @param Request       $request
     * @param BinanceClient $binanceClient
     * @return Response
     */
    public function depth(Request $request, BinanceClient $binanceClient): Response
    {
        try {
            return $this->handleView(
                $this->view(
                    $binanceClient->depth(
                        $request->get('symbol')
                    )
                )
            );
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }

    /**
     * @Rest\Get("/book-price")
     * @param Request       $request
     * @param BinanceClient $binanceClient
     * @return Response
     */
    public function bookPrices(Request $request, BinanceClient $binanceClient): Response
    {
        try {
            return $this->handleView(
                $this->view(
                    $binanceClient->bookPrice(
                        $request->get('symbol')
                    )
                )
            );
        } catch (\Exception $exception) {
            return $this->exceptionResponse($exception);
        }
    }
}
